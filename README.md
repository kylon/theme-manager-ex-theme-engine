## Theme Manager Ex - The only theme engine for vita
Theme Manager Ex is a theme engine for PS Vita that introduces a new type of custom themes: Extended Themes.  

Features:  

* Manage your themes
* Apply and unset Official themes
* Apply and unset Custom themes
* Supports Extended Themes

Controls:  

Up, Down, Left, Right - Move cursor  
Triangle - Open menu / Close menu  
Circle - Go back / Cancel  
Cross - Enter / Accept  
Square - Apply theme  


Here you can find a few sample themes, their purpouse is to show you what you will be able to do with Theme Manager Ex.

[Extended Themes Preview](https://bitbucket.org/kylon/extended-themes)  
[Theme Manager Ex Wiki](https://bitbucket.org/kylon/theme-manager-ex/wiki/Home)  

## Credits
Team Molecule: Henkaku  
Xerpi: vita2d  
xyz: Sqlite3  
TheFlow: VitaShell code  
dots-tb and CelesteBlue-dev: PSVita-RE-tools  
VitaSmith: libsqlite  
All iirc members for the help  
