#include <psp2/system_param.h>

#include "include/utils.h"
#include "include/graphics.h"

void themeManagerExInit() {
	int ret;

	if (!exists("ux0:theme")) {
		ret = mkDir("ux0:theme", 0777);
		if (!ret) {
			thmx_dialog(0, "INIT: Cannot create theme folder");
			sceKernelExitProcess(1);
		}
	}

	if (!exists(THMX_DATA)) {
		ret = mkDir(THMX_DATA, 0777);
		if (!ret) {
			thmx_dialog(0, "INIT: Cannot create thmx folder");
			sceKernelExitProcess(1);
		}
	}

	ret = sceRegMgrGetKeyInt("/CONFIG/SYSTEM", "language", &resources.sysLanguage);
	if (ret < 0) {
		thmx_dialog(0, "Cannot read registry");
		sceKernelExitProcess(1);
	}

	return;
}

char **getThemesListAndCount(int *totThemes) {
	char **arr = malloc(sizeof(char *) * 1);
	SceUID dfd = sceIoDopen("ux0:theme");
	int ret = 0, i = 0, sz = 1;

	if (dfd < 0 || arr == NULL) {
		sceIoDclose(dfd);
		free(arr);
		thmx_dialog(0, "INIT: Cannot create themes list!");
		sceKernelExitProcess(1);
	}

	do {
		SceIoDirent dir;
		size_t len;

		memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0)
			break;

		if (FAST_STRCMP(dir.d_name, ".") || FAST_STRCMP(dir.d_name, "..") || !SCE_S_ISDIR(dir.d_stat.st_mode))
			continue;

		int dNameL = strlen(dir.d_name);
		int pLen = dNameL + 21;
		char xmlPath[pLen];

		snprintf(xmlPath, sizeof(char) * pLen, "ux0:theme/%s/theme.xml", dir.d_name);
		if (!exists(xmlPath))
			continue;

		if (i == sz) {
			sz *= 2;
			arr = realloc(arr, sizeof(char *) * sz);

			if (arr == NULL) {
				sceIoDclose(dfd);
				thmx_dialog(0, "INIT: Memory allocation error!");
				sceKernelExitProcess(1);
			}
		}

		len = sizeof(char) * (dNameL + 1);
		arr[i] = malloc(len);

		snprintf(arr[i], len, "%s", dir.d_name);
		++i;
	} while (ret > 0);

	sceIoDclose(dfd);

	(*totThemes) = i;

	return arr;
}

int getThemeSize(const char *themePath) {
	int size = 0, ret = 0;
	SceUID dfd = sceIoDopen(themePath);

	if (dfd < 0)
		return 0;

	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0)
			break;

		if (FAST_STRCMP(dir.d_name, ".") || FAST_STRCMP(dir.d_name, ".."))
			continue;

		if (SCE_S_ISDIR(dir.d_stat.st_mode)) {
			int len = strlen(themePath) + strlen(dir.d_name) + 2;
			char path[len];

			snprintf(path, sizeof(char) * len, "%s/%s", themePath, dir.d_name);

			size += getThemeSize(path);
		} else {
			size += dir.d_stat.st_size;
		}
	} while (ret > 0);

	sceIoDclose(dfd);

	return size;
}

xmlChar *parseNodes(xmlNode * a_node, const char *what) {
	xmlNode *node = NULL;
	xmlChar *ret = NULL;

	for (node = a_node; node; node = node->next) {
		if (node->type != XML_ELEMENT_NODE || node->name[0] != 'm')
			goto for_continue;

		if (FAST_STRCMP(what, "thumb") && FAST_STRCMP(node->name, "m_packageImageFilePath") ||
					FAST_STRCMP(what, "homePrew") && FAST_STRCMP(node->name, "m_homePreviewFilePath") ||
					FAST_STRCMP(what, "lockPrew") && FAST_STRCMP(node->name, "m_startPreviewFilePath") ||
					FAST_STRCMP(what, "version") && FAST_STRCMP(node->name, "m_contentVer")) {

			return xmlNodeGetContent(node);

		} else if (FAST_STRCMP(what, "author") && FAST_STRCMP(node->name, "m_provider") ||
					FAST_STRCMP(what, "title") && FAST_STRCMP(node->name, "m_title")) {

			return FAST_STRCMP(node->children->name, "m_default") ?
							xmlNodeGetContent(node->children) :
							xmlNodeGetContent(node->children->next);

		} else if (FAST_STRCMP(what, "browser") && FAST_STRCMP(node->name, "m_browser") ||
					FAST_STRCMP(what, "trophies") && FAST_STRCMP(node->name, "m_trophy") ||
					FAST_STRCMP(what, "friends") && FAST_STRCMP(node->name, "m_friend") ||
					FAST_STRCMP(what, "messages") && FAST_STRCMP(node->name, "m_message")||
					FAST_STRCMP(what, "party") && FAST_STRCMP(node->name, "m_party") ||
					FAST_STRCMP(what, "ps4link") && FAST_STRCMP(node->name, "m_ps4Link") ||
					FAST_STRCMP(what, "parentalctrl") && FAST_STRCMP(node->name, "m_parental") ||
					FAST_STRCMP(what, "music") && FAST_STRCMP(node->name, "m_music") ||
					FAST_STRCMP(what, "photos") && FAST_STRCMP(node->name, "m_camera") ||
					FAST_STRCMP(what, "email") && FAST_STRCMP(node->name, "m_email") ||
					FAST_STRCMP(what, "calendar") && FAST_STRCMP(node->name, "m_calendar") ||
					FAST_STRCMP(what, "cma") && FAST_STRCMP(node->name, "m_hostCollabo") ||
					FAST_STRCMP(what, "near") && FAST_STRCMP(node->name, "m_near") ||
					FAST_STRCMP(what, "ps3link") && FAST_STRCMP(node->name, "m_ps3Link") ||
					FAST_STRCMP(what, "settings") && FAST_STRCMP(node->name, "m_settings") ||
					FAST_STRCMP(what, "videos") && FAST_STRCMP(node->name, "m_video")) {

			return FAST_STRCMP(node->children->name, "m_iconFilePath") ?
							xmlNodeGetContent(node->children) :
							xmlNodeGetContent(node->children->next);
		}

for_continue:
		ret = parseNodes(node->children, what);
		if (ret != NULL)
			return ret;
	}

	return ret;
}

xmlChar *getThemeProp(xmlDoc *doc, const char *what) {
	xmlChar *ret = NULL;
	xmlNode *root = NULL;

	root = xmlDocGetRootElement(doc);
	ret = parseNodes(root, what);

	if (ret == NULL) {
		xmlFree(ret);

		size_t len = sizeof(xmlChar) * 8;
		xmlChar *prop = malloc(len);

		snprintf(prop, len, "Unknown");
		return prop;
	}

	return ret;
}

char *getThemeXmlPath(const char *themeFolder, int ex) {
	size_t len = sizeof(char) * (strlen(themeFolder) + 24);
	char *themeXmlPath = malloc(len);

	snprintf(themeXmlPath, len, "ux0:theme/%s/theme%s.xml", themeFolder, ex ? "Ex":"");

	return themeXmlPath;
}

xmlChar *getThemeTitle(xmlDoc *doc) {
	xmlChar *ret = getThemeProp(doc, "title");

	if (ret == NULL) {
		xmlFree(ret);

		size_t len = sizeof(xmlChar) * 8;
		xmlChar *unkT = malloc(len);

		snprintf(unkT, len, "Unknown");
		return unkT;
	}

	return ret;
}

xmlChar *getThemePreviewPng(const char *themeFolder, xmlDoc *doc, const char *imageType) {
	xmlChar *image = getThemeProp(doc, imageType);
	size_t retLen = sizeof(xmlChar) * (strlen(themeFolder) + xmlStrlen(image) + 12);
	xmlChar *ret = malloc(retLen);

	snprintf(ret, retLen, "ux0:theme/%s/%s", themeFolder, image);
	xmlFree(image);

	return ret;
}

int delTheme(const char *id, int installed) {
	int ret;

	draw_progress(10, "Doing some checks...");

	if (installed) {
		sqlite3 *db;
		char sql[256] = {0};

		draw_progress(20, "Opening database...");

		ret = sqlite3_open(APPDB, &db);
		if (ret != SQLITE_OK) {
			int len = strlen(id) + 60;
			char er[len];

			snprintf(er, sizeof(char) * len, "Cannot open the database!\n\nID: %s\n\nErr: %d", id, ret);
			sqlite3_close(db);
			thmx_dialog(0, er);
			return 0;
		}

		draw_progress(50, "Deleting theme...");
		snprintf(sql, sizeof(sql), "DELETE FROM tbl_theme WHERE id = '%s'", id);

		ret = sql_exec(db, sql, "deleteTheme");
		if (!ret) {
			int len = strlen(id) + 40;
			char msg[len];

			snprintf(msg, sizeof(char) * len, "Cannot execute delete query!\n\nID: %s", id);
			sqlite3_close(db);
			thmx_dialog(0, msg);
			return 0;
		}

		sqlite3_close(db);
	}

	draw_progress(80, "Deleting theme...");

	ret = ioRemove(id);
	if (!ret) {
		int len = strlen(id) + 40;
		char er[len];

		snprintf(er, sizeof(char) * len, "Cannot delete theme files!\n\nPath: %s", id);
		thmx_dialog(0, er);
		return 0;
	}

	draw_progress(100, "Done.");

	return 1;
}

int manageDB(int cmd) {
	int ret;

	switch (cmd) {
		case 1: { // backup
			char *bakPath = THMX_DATA"/shell_bak/app.db";

			if (!exists(THMX_DATA"/shell_bak"))
				mkDir(THMX_DATA"/shell_bak", 0777);

			ret = copyFile(APPDB, bakPath);
			if (!ret)
				return 0;
		}
			break;
		case 2: { // restore
			char *bakPath = THMX_DATA"/shell_bak/app.db";

			if (!exists(bakPath))
				return 0;

			ret = copyFile(bakPath, APPDB);
			if (!ret)
				return 0;
		}
			break;
		case 3: { // rebuild
			char *bakPath = THMX_DATA"/shell_bak/id.dat";

			if (!exists(THMX_DATA"/shell_bak"))
				mkDir(THMX_DATA"/shell_bak", 0777);

			ret = copyFile("ux0:id.dat", bakPath);
			if (!ret)
				return 0;

			ret = ioRemove("ux0:id.dat");
			if (!ret)
				return 0;
		}
			break;
		default:
			break;
	}

	return 1;
}

char *createTimeStamp() {
	struct tm * timeInfo;
	time_t rawtime;
	char *ret;

	time(&rawtime);
	timeInfo = localtime(&rawtime);

	ret = malloc(sizeof(char) * 30);
	strftime(ret, 30, "%Y-%m-%d %H:%M:%S.000", timeInfo);

	return ret;
}

char *genSqliteBlob(const char *str) {
	size_t retLen = sizeof(char) * 250;
	int isEmptyRet = 1;
	char *ret;

	ret = malloc(retLen);

	for (int i=0, len=strlen(str); i<len; ++i) {
		if (str[i] == 32 || (str[i] >= 48 && str[i] <= 57) || (str[i] >= 65 && str[i] <= 90) || (str[i] >= 97 && str[i] <= 122)) {

			if (isEmptyRet && str[i] != 32)
				isEmptyRet = 0;

			if (i == 0)
				snprintf(ret, retLen, "%02X00", str[i]);
			else
				snprintf(ret, retLen, "%s%02X00", ret, str[i]);
		}
	}

	if (isEmptyRet) {
		free(ret);
		ret = NULL;
	}

	return ret;
}

int genVersionNumb(const char *str) {
	char version[20] = {0};
	int major, minor, num;

	sscanf(str, "%d.%d", &major, &minor);
	snprintf(version, sizeof(version), "%d%d", major, minor);

	num = atoi(version);
	if (num < 100)
		num *= 10;

	return num;
}

int installAll(char **themesList, int themesCount) {
	sqlite3 *db;
	int errC = 0, prog = 50, progInc = 0;
	int ret;

	draw_progress(10, "Installation will start shortly...");

	ret = sqlite3_open(APPDB, &db);
	if (ret != SQLITE_OK) {
		char msg[40] = {0};

		snprintf(msg, sizeof(msg), "Cannot open the database!\n\nError: %d", ret);
		sqlite3_close(db);
		thmx_dialog(0, msg);
		return 0;
	}

	progInc = floor(countFolders("ux0:theme") / 80);
	for (int i=0; i<themesCount; ++i) {
		int themeStrLen = strlen(themesList[i]);
		int msgLen = themeStrLen + 15;
		int idLen = themeStrLen + 12;
		int sqlLen = idLen + 50;
		char msg[msgLen], id[idLen], sqlCnt[sqlLen];
		sqlite3_stmt *stmt;
		int installed = 0;
		char *themeXmlPath;
		xmlDoc *doc;

		prog += progInc;

		snprintf(id, sizeof(char) * idLen, "ux0:/theme/%s", themesList[i]);
		sqlite3_snprintf(sizeof(char) * sqlLen, sqlCnt, "SELECT COUNT(*) FROM tbl_theme WHERE id = '%q'", id);

		ret = sqlite3_prepare_v2(db, sqlCnt, -1, &stmt, 0);
		if (ret != SQLITE_OK) {
#ifdef THMXD
			const char *sqlEr = sqlite3_errmsg(db);

			sceClibPrintf("installAll: sql error: %s, ID: %s\nquery: %s\n", sqlEr, id, sqlCnt);
#endif
			++errC;
			continue;
		}

		if (sqlite3_step(stmt) == SQLITE_ROW)
			installed = sqlite3_column_int(stmt, 0);

		sqlite3_finalize(stmt);

		if (installed)
			continue;

		themeXmlPath = getThemeXmlPath(themesList[i], 0);
		doc = xmlReadFile(themeXmlPath, NULL, 0);

		if (doc == NULL) {
#ifdef THMXD
			sceClibPrintf("installAll: Cannot open theme.xml: %s\n", themeXmlPath);
#endif
			free(themeXmlPath);
			++errC;
			continue;
		}

		snprintf(msg, sizeof(char) * msgLen, "Installing %s...", themesList[i]);
		draw_progress(prog, msg);

		xmlChar *themeThumb = getThemeProp(doc, "thumb");
		xmlChar *lockPrew = getThemeProp(doc, "lockPrew");
		xmlChar *homePrew = getThemeProp(doc, "homePrew");
		xmlChar *version = getThemeProp(doc, "version");
		xmlChar *author = getThemeProp(doc, "author");
		xmlChar *title = getThemeTitle(doc);
		char *timeStamp = createTimeStamp();
		char *authorBlob = genSqliteBlob(author);
		char *titleBlob = genSqliteBlob(title);
		int versionNumb = genVersionNumb(version);
		int themeSize = getThemeSize(id);

		if (authorBlob == NULL) {
			free(authorBlob);
			authorBlob = genSqliteBlob("Unknown");
		}

		if (titleBlob == NULL) {
			free(titleBlob);
			titleBlob = genSqliteBlob(themesList[i]); // Use the folder name if the title cannot be read.
		}

		sqlLen = 1200 + strlen(id) + xmlStrlen(themeThumb) + xmlStrlen(lockPrew) + xmlStrlen(homePrew) + strlen(titleBlob) + strlen(authorBlob) + strlen(timeStamp);
		char sqlInstall[sqlLen];

		sqlite3_snprintf(sizeof(char) * sqlLen, sqlInstall,
				"INSERT INTO tbl_theme (reserved05, reserved04, reserved03, reserved02, reserved01, provider25, title25, provider24, title24, provider23, title23, provider22, title22, provider21, title21, provider20, title20, provider19, title19, provider18, title18, provider17, title17, provider16, title16, provider15, title15, provider14, title14, provider13, title13, provider12, title12, provider11, title11, provider10, title10, provider9, title9, provider8, title8, provider7, title7, provider6, title6, provider5, title5, provider4, title4, provider3, title3, provider2, title2, provider1, title1, provider0, title0, providerDefault, titleDefault, type, size, lastModifiedTime, contentVer, startPreviewFilePath, homePreviewFilePath, packageImageFilePath, id) VALUES ( NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, X'%q', X'%q', 100, %d, '%q', %d, '%q', '%q', '%q', '%q')",
				authorBlob, titleBlob ,themeSize, timeStamp, versionNumb, lockPrew, homePrew, themeThumb, id);

		xmlFree(themeThumb);
		xmlFree(lockPrew);
		xmlFree(homePrew);
		xmlFree(author);
		xmlFree(version);
		xmlFree(title);
		free(themeXmlPath);
		free(timeStamp);
		free(titleBlob);
		free(authorBlob);
		xmlFreeDoc(doc);

		ret = sql_exec(db, sqlInstall, "installAll: sqlInstall");
		if (!ret) {
			++errC;
			continue;
		}
	}

	draw_progress(90, "Closing database...");
	sqlite3_close(db);

	if (errC) {
		char msg[91] = {0};

		snprintf(msg, sizeof(msg), "Process completed with %d errors\n\nIf the problem persists, please contact the developer.", errC);
		thmx_dialog(2, msg);
	}

	return 1;
}

int updateThemesList(char **themesList, int themesCount, int deletedIdx) {
	if (themesList[deletedIdx] != NULL) {
		free(themesList[deletedIdx]);
		themesList[deletedIdx] = NULL;
	}

	for (int i=deletedIdx, len=themesCount-1; i<len; ++i)
		themesList[i] = themesList[i+1];

	return themesCount-1;
}

int applyTheme(const char *themeFolder) {
	const char *sqlPageCount = "SELECT COUNT(*) FROM tbl_appinfo_page WHERE pageNo >= 0";
	xmlNode *root = NULL, *node = NULL;
	int themeFoldLen = strlen(themeFolder);
	int themePathRegLen = themeFoldLen + 13;
	char themePathForReg[themePathRegLen];
	char *idsList[16] = { // Safely recycle EX IDs for this function
		"browser", "trophies", "friends", "messages", "party", "ps4link",
	    "parentalctrl", "music", "photos", "email", "calendar", "cma",
	    "near", "ps3link", "settings", "videos"
	};
	char *themeXmlPath = getThemeXmlPath(themeFolder, 0);
	xmlDoc *doc = xmlReadFile(themeXmlPath, NULL, 0);
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int pageCount = 0, errC = 0;
	int pgNo, ret;

#ifdef THMXD
	sceClibPrintf("apply %s, %s\n", themeXmlPath, themeFolder);
#endif
	draw_progress(10, "Loading XML...");
	free(themeXmlPath);

	if (doc == NULL) {
		int len = strlen(themeFolder) + 34;
		char msg[len];

		snprintf(msg, sizeof(char) * len, "Apply: Cannot open theme.xml!\n\n%s", themeFolder);
		thmx_dialog(0, msg);
		return 0;
	}

	draw_progress(30, "Opening database...");

	ret = sqlite3_open(APPDB, &db);
	if (ret != SQLITE_OK) {
		char msg[40] = {0};

		snprintf(msg, sizeof(msg), "Cannot open the database!\n\nError: %d", ret);
		sqlite3_close(db);
		xmlFreeDoc(doc);
		thmx_dialog(0, msg);
		return 0;
	}

	snprintf(themePathForReg, sizeof(char) * themePathRegLen, "ux0:/theme/%s", themeFolder);
#ifdef THMXD
	sceClibPrintf("themepathreg %s\n", themePathForReg);
#endif
	draw_progress(50, "Setting icons...");

	for (int i=0; i<16; ++i) {
		xmlChar *iconName = getThemeProp(doc, idsList[i]);
		char *curId = getTitleId(idsList[i]);
		int iconLen = xmlStrlen(iconName);
		int msgLen = iconLen + 12;
		int sqlLen = themePathRegLen + iconLen + strlen(curId) + 70;
		char msg[msgLen];
		char sqlIcon[sqlLen];

		if (FAST_STRCMP(iconName, "Unknown"))
			goto for_continue;

		snprintf(msg, sizeof(char) * msgLen, "Setting %s...", iconName);
		draw_progress(50, msg);

		snprintf(sqlIcon, sizeof(char) * sqlLen, "UPDATE tbl_appinfo_icon SET reserved02 = '%s/%s' WHERE titleId = '%s'", themePathForReg, iconName, curId);

#ifdef THMXD
		sceClibPrintf("icon sett: %s, %s\n", iconName, sqlIcon);
		sceClibPrintf("idList[%d]=%s\nFound icon: %s\nFound Icon for ID: %s\n", i, idsList[i], iconName, curId);
#endif

		ret = sql_exec(db, sqlIcon, "applyTheme: icon");
		if (!ret)
			++errC;

for_continue:
		xmlFree(iconName);
		free(curId);
	}

	// Set bgs and waves
	root = xmlDocGetRootElement(doc);
	node = root->children;

#ifdef THMXD
	sceClibPrintf("Loading bgs and waves\n\n");
	sceClibPrintf("sqlpagecount: %s\n", sqlPageCount);
#endif

	ret = sqlite3_prepare_v2(db, sqlPageCount, -1, &stmt, 0);
	if (ret != SQLITE_OK) {
		const char *sqlEr = sqlite3_errmsg(db);
		int msgLen = strlen(sqlEr) + 34;
		char msg[msgLen];

		snprintf(msg, sizeof(char) * msgLen, "Cannot execute query!\nError:\n%s", sqlEr);

		xmlFreeDoc(doc);
		sqlite3_finalize(stmt);
		sqlite3_close(db);
		unsetTheme();
		thmx_dialog(0, msg);
		return 0;
	}

	if (sqlite3_step(stmt) == SQLITE_ROW)
		pageCount = sqlite3_column_int(stmt, 0);

	sqlite3_finalize(stmt);
#ifdef THMXD
	sceClibPrintf("pagecount %d\n", pageCount);
#endif

	for (; node; node = node->next) {
		if (node->type != XML_ELEMENT_NODE || !FAST_STRCMP(node->name, "HomeProperty"))
			continue;

		while (!FAST_STRCMP(node->name, "BackgroundParam"))
			node = node->children != NULL ? node->children : node->next;

		draw_progress(80, "Setting bgs and waves...");

		pgNo = 0;
		while (pgNo < pageCount && node != NULL) {
			if (node->type != XML_ELEMENT_NODE || !FAST_STRCMP(node->name, "BackgroundParam")) {
				node = node->next;
				continue;
			}

			char sqlBg[256] = {0};
			xmlNode *tmp = node->children;
			xmlChar *bgImage = NULL;
			int waveType = 0;

			while (tmp) {
				if (node->type != XML_ELEMENT_NODE) {
					tmp = tmp->next;
					continue;
				}

				if (FAST_STRCMP(tmp->name, "m_imageFilePath")) {
					bgImage = xmlNodeGetContent(tmp);
#ifdef THMXD
					sceClibPrintf("Found bgImage: %s\n", bgImage);
#endif
				} else if (FAST_STRCMP(tmp->name, "m_waveType")) {
					xmlChar *waveT = xmlNodeGetContent(tmp);

					if (waveT == NULL) {
						waveType = 0;

					} else {
						waveType = atoi(waveT);

						if (waveType <= 0)
							waveType = 10;
					}
					xmlFree(waveT);
#ifdef THMXD
					sceClibPrintf("Found wave: %d\n", waveType);
#endif
				}
				tmp = tmp->next;
			}

			if (bgImage != NULL) {
				int len = xmlStrlen(bgImage) + themeFoldLen + 12;
				char bgImPath[len];

				snprintf(bgImPath, sizeof(char) * len, "ux0:theme/%s/%s", themeFolder, bgImage);

				sqlite3_snprintf(sizeof(sqlBg), sqlBg,
									"UPDATE tbl_appinfo_page SET themeFile = %Q, bgColor = %d, texWidth = %d, texHeight = %d, imageWidth = %d, imageHeight = %d WHERE pageNo = %d",
									bgImPath, waveType, 960, 512, 960, 512, pgNo);
			} else {
				sqlite3_snprintf(sizeof(sqlBg), sqlBg,
									"UPDATE tbl_appinfo_page SET themeFile = NULL, bgColor = %d, texWidth = 0, texHeight = 0, imageWidth = 0, imageHeight = 0 WHERE pageNo = %d",
									waveType, pgNo);
			}
#ifdef THMXD
			sceClibPrintf("bg query %s\n", sqlBg);
#endif
			ret = sql_exec(db, sqlBg, "applyTheme: sqlBg");
			if (!ret)
				++errC;

			xmlFree(bgImage);
			++pgNo;

			node = node->next;
		}
		break;
	}

	// If we find more pages but no backgrounds/waves, use default values
	while (pgNo < pageCount) {
		char sqlPgNo[140] = {0};

		sqlite3_snprintf(sizeof(sqlPgNo), sqlPgNo,
		         "UPDATE tbl_appinfo_page SET themeFile = NULL, bgColor = 0, texWidth = 0, texHeight = 0, imageWidth = 0, imageHeight = 0 WHERE pageNo = %d",
		         pgNo);

		sql_exec(db, sqlPgNo, "applyTheme: pagecount");
#ifdef THMXD
		sceClibPrintf("sqlpgno: %s\n", sqlPgNo);
#endif
		++pgNo;
	}

	xmlFreeDoc(doc);
	sqlite3_close(db);

	draw_progress(90, "Setting theme flag...");

	// 2 -> Tell the system we are using a custom lockscreen
	ret = sceRegMgrSetKeyInt("/CONFIG/THEME", "wallpaper", 2);
	ret = sceRegMgrSetKeyStr("/CONFIG/THEME", "current_theme", themePathForReg, themePathRegLen+1);

	if (ret < 0) {
		unsetTheme();
		thmx_dialog(0, "Cannot set registry keys");
		return 0;
	}

	return 1;
}

int applyExTheme(const char *themeFolder) {
	xmlChar *themeExXmlPath = getThemeXmlPath(themeFolder, 1);
	xmlDoc *doc = xmlReadFile(themeExXmlPath, NULL, 0);
	int themeFoldLen = strlen(themeFolder);
	int thmxDataLen = strlen(THMX_DATA);
	xmlNode *root = NULL;
	sqlite3 *db = NULL;
	FILE *activeF = NULL;
	int isVs0Mounted = 0;
	int errC = 0;
	int ret;

	xmlFree(themeExXmlPath);
	draw_progress(10, "Loading XML...");

	if (doc == NULL) {
		int len = themeFoldLen + 35;
		char msg[len];

		snprintf(msg, sizeof(char) * len, "Apply: Cannot open themeEx.xml!\n%s", themeFolder);

		unsetTheme();
		thmx_dialog(0, msg);
		return 0;
	}

	draw_progress(15, "Doing some checks...");

	if (!exists(THMX_DATA"/active")) {
		ret = mkDir(THMX_DATA"/active", 0777);
		if (!ret) {
			xmlFreeDoc(doc);
			unsetTheme();
			thmx_dialog(0, "Cannot create override folder!");
			return 0;
		}
	}

	draw_progress(30, "Preparing files...");

	activeF = fopen(THMX_DATA"/active.txt", "w");
	if (activeF == NULL) {
		xmlFreeDoc(doc);
		unsetTheme();
		thmx_dialog(0, "Cannot create thmx backup files!");
		return 0;
	}

	ret = sqlite3_open(APPDB, &db);
	if (ret != SQLITE_OK) {
		char msg[40] = {0};

		snprintf(msg, sizeof(msg), "Cannot open the database!\n\nError: %d", ret);

		sqlite3_close(db);
		fclose(activeF);
		xmlFreeDoc(doc);
		unsetTheme();
		thmx_dialog(0, msg);
		return 0;
	}

	sqlite3_create_function(db, "add_rm_list", 1, SQLITE_UTF8, NULL, &_add_rm_list, NULL, NULL);

	root = xmlDocGetRootElement(doc);
	for (xmlNode *node = root->children; node; node = node->next) {
		xmlChar *titleColor, *overrideLva, *instLva, *overrideSplash, *styleOverride, *instTemplate;
		char *titleId = getTitleId(node->name);
		int tidLen = strlen(titleId);
		int templateOvrd = 0, laEmpty = 0;
		uint titleColorBak = 0;
		char laStyleBak[16] = {0};

		if (node->type != XML_ELEMENT_NODE || titleId == NULL) {
			free(titleId);
			continue;
		}

		titleColor = xmlGetProp(node, "title-color");
		overrideLva = xmlGetProp(node, "override-livearea");
		instLva = xmlGetProp(node, "install-livearea");
		overrideSplash = xmlGetProp(node, "override-splash");
		styleOverride= xmlGetProp(node, "style");
		instTemplate = xmlGetProp(node, "install-template");

		// Extended Standard Element - the only one with an icon
		if (node->name[0] == 'm') {
			xmlChar *img = xmlNodeGetContent(node);
			int imgLen = themeFoldLen + xmlStrlen(img) + 22;
			int sqlLen = imgLen + tidLen + 64;
			char imgPath[imgLen];
			char sql[sqlLen];

			if (img != NULL) {
				int msgLen = tidLen + 21;
				char msg[msgLen];

				snprintf(msg, sizeof(char) * msgLen, "Installing Icon: %s...", titleId);
				draw_progress(50, msg);

				snprintf(imgPath, sizeof(char) * imgLen, "ux0:/theme/%s/extended/%s", themeFolder, img);
				sqlite3_snprintf(sizeof(char) * sqlLen, sql, "UPDATE tbl_appinfo_icon SET reserved02 = '%q' WHERE titleId = '%s'", imgPath, titleId);
				xmlFree(img);

				ret = sql_exec(db, sql, "applyExTheme: icon");
				if (!ret) {
					++errC;
					goto for_continue;
				}
			}
		}

		// Custom title color
		if (titleColor != NULL && instLva == NULL) {
			int sqlSvLen = tidLen + 55;
			int sqlTcLen = tidLen + 70;
			int msgLen = tidLen + 30;
			char sqlTc[sqlTcLen];
			char sqlSave[sqlSvLen];
			char msgT[msgLen];
			sqlite3_stmt *stmt;
			uint color;

			snprintf(msgT, sizeof(char) * msgLen, "Installing title color: %s...", titleId);
			draw_progress(50, msgT);

			snprintf(sqlSave, sizeof(char) * sqlSvLen, "SELECT titleColor FROM tbl_livearea WHERE titleId = '%s'", titleId);

			ret = sqlite3_prepare_v2(db, sqlSave, -1, &stmt, 0);
			if (ret != SQLITE_OK) {
#ifdef THMXD
				const char *sqlEr = sqlite3_errmsg(db);

				sceClibPrintf("applyExTheme: Title color: sql save error!\nID: %s\nErr: %s\n", titleId, sqlEr);
#endif
				++errC;
				sqlite3_finalize(stmt);
				goto for_continue;
			}

			if (sqlite3_step(stmt) == SQLITE_ROW)
				titleColorBak = sqlite3_column_int(stmt, 0);

			sqlite3_finalize(stmt);

			sscanf(titleColor, "%x", &color);
			sqlite3_snprintf(sizeof(char) * sqlTcLen, sqlTc, "UPDATE tbl_livearea SET titleColor = %u WHERE titleId = '%s'", color, titleId);

			ret = sql_exec(db, sqlTc, "applyExTheme: ovrdTitleColor");
			if (!ret) {
				++errC;
				goto for_continue;
			}
		}

		// Custom Style
		if (styleOverride != NULL && instLva == NULL) {
			int sqlSvLen = tidLen + 50;
			int sqlStyleLen = tidLen + + xmlStrlen(styleOverride) + 56;
			int msgLen = tidLen + 30;
			char sqlSvStyle[sqlSvLen];
			char sqlStyle[sqlStyleLen];
			char msgS[msgLen];
			sqlite3_stmt *stmt;

			snprintf(msgS, sizeof(char) * msgLen, "Installing custom style: %s...", titleId);
			draw_progress(50, msgS);

			snprintf(sqlSvStyle, sizeof(char) * sqlSvLen, "SELECT style FROM tbl_livearea WHERE titleId = '%s'", titleId);

			ret = sqlite3_prepare_v2(db, sqlSvStyle, -1, &stmt, 0);
			if (ret != SQLITE_OK) {
#ifdef THMXD
				const char *sqlEr = sqlite3_errmsg(db);

				sceClibPrintf("applyExTheme: custom style: save error!\nID: %s\nErr: %s\n", titleId, sqlEr);
#endif
				++errC;
				sqlite3_finalize(stmt);
				goto for_continue;
			}

			if (sqlite3_step(stmt) == SQLITE_ROW)
				sqlite3_snprintf(sizeof(laStyleBak), laStyleBak, "%s", sqlite3_column_text(stmt, 0));

			sqlite3_finalize(stmt);
			snprintf(sqlStyle, sizeof(char) * sqlStyleLen, "UPDATE tbl_livearea SET style = '%s' WHERE titleId = '%s'", styleOverride, titleId);

			ret = sql_exec(db, sqlStyle, "applyExTheme: ovrdStyle");
			if (!ret) {
				++errC;
				goto for_continue;
			}
		}

		// Custom Template
		if (instTemplate != NULL && FAST_STRCMP(instTemplate, "true")) {
			int pathLen = themeFoldLen + tidLen + 34;
			int msgLen = tidLen + 30;
			char templatePath[pathLen];
			char msgTT[msgLen];

			snprintf(msgTT, sizeof(char) * msgLen, "Installing template: %s...", titleId);
			draw_progress(50, msgTT);

			// Get template path
			snprintf(templatePath, sizeof(char) * pathLen, "ux0:theme/%s/extended/%s/template.xml", themeFolder, titleId);

			if (exists(templatePath)) {
				char *langStr = getLangString();

				templateOvrd = 1;
				ret = parseAndInstallTemplate(templatePath, titleId, db, langStr);

				free(langStr);

				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot parse template\n");
#endif
					++errC;
					goto for_continue;
				}
			}
		}

		// Install Live Area (for empty *USER* apps)
		if (instLva != NULL && FAST_STRCMP(instLva, "true")) {
			int checkLen = tidLen + 57;
			int lvaLen = tidLen + 31;
			int pathLen = tidLen + 13;
			int instLen = xmlStrlen(styleOverride) + tidLen + lvaLen + 510;
			int instPathLen = themeFoldLen + tidLen + 30;
			int msgLen = tidLen + 30;
			char sqlCheck[checkLen];
			char ur0Path[pathLen];
			char ur0LvaPath[lvaLen];
			char sqlILva[instLen];
			char instLvaPath[instPathLen];
			char msgLva[msgLen];
			sqlite3_stmt *stmt;
			int appCount = 0;

			snprintf(msgLva, sizeof(char) * msgLen, "Installing Live Area: %s...", titleId);
			draw_progress(50, msgLva);

			snprintf(sqlCheck, sizeof(char) * checkLen, "SELECT COUNT(*) FROM tbl_appinfo_icon WHERE titleId = '%s'", titleId);
			snprintf(ur0LvaPath, sizeof(char) * lvaLen, "ur0:appmeta/%s/livearea/contents", titleId);
			snprintf(ur0Path, sizeof(char) * pathLen, "ur0:appmeta/%s", titleId);

			ret = sqlite3_prepare_v2(db, sqlCheck, -1, &stmt, 0);
			if (ret != SQLITE_OK) {
#ifdef THMXD
				sceClibPrintf("applyExTheme: Cannot execute InstLva query ID: %s\nError: %s\nquery: %s\n", titleId, ret, sqlCheck);
#endif
				++errC;
				goto for_continue;
			}

			if (sqlite3_step(stmt) == SQLITE_ROW)
				appCount = sqlite3_column_int(stmt, 0);

			sqlite3_finalize(stmt);

			if (appCount == 0) {
				ioRemove(ur0Path);
				mkDir(ur0Path, 0777);
			}

			if (!exists(ur0LvaPath) && exists(ur0Path)) {
				laEmpty = 1;

				sqlite3_snprintf(sizeof(char) * instLen, sqlILva,
							"INSERT INTO tbl_livearea (pccSignOff, pccResStatus, pccUpdDate, sfTarg7, sfType7, sfTarg6, sfType6, sfTarg5, sfType5, sfTarg4, sfType4, sfTarg3, sfType3, sfTarg2, sfType2, sfTarg1, sfType1, sfTarg0, sfType0, modifiedDate, titleColor, gate_startupImage, background_image, contentRev, formatVer, style, org_path, titleId) VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'startup.png', 'bg.png', 1, '01.00', '%q', '%q', '%q');",
							styleOverride == NULL ? "a1":(char *)styleOverride, ur0LvaPath, titleId);

				ret = sql_exec(db, sqlILva, "applyExTheme: instLva");
				if (!ret) {
					++errC;
					goto for_continue;
				}

				if (titleColor != NULL) {
					char colorSql[50] = {0};

					sqlite3_snprintf(sizeof(char) * 20, colorSql,
							"UPDATE tbl_livearea SET titleColor = %u WHERE titleId = '%s';",
							titleColor, titleId);

					sql_exec(db, colorSql, "applyExTheme: instLva: titleColor");
				}

				ret = mkDir(ur0LvaPath, 0777);
				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot create live area folders for: %s\n", titleId);
#endif
					++errC;
					goto for_continue;
				}

				snprintf(instLvaPath, sizeof(char) * instPathLen, "ux0:theme/%s/extended/%s/contents", themeFolder, titleId);

				ret = copyDir(instLvaPath, ur0LvaPath);
				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot install live area for: %s\n", titleId);
#endif
					++errC;
					goto for_continue;
				}

			} else {
#ifdef THMXD
				sceClibPrintf("applyExTheme: Invalid option for: %s, use override-livearea instead!\n", titleId);
#endif
			}
		} else if (overrideLva != NULL && FAST_STRCMP(overrideLva, "true")) { // Custom Live Area
			int ovrdLen = themeFoldLen + tidLen + 30;
			int ur0PLen = tidLen + 32;
			int vs0PLen = tidLen + 35;
			int orgPLen = tidLen + 36;
			int orgPBakLen = tidLen + thmxDataLen + 28;
			int msgLen = tidLen + 34;
			char ovrdPath[ovrdLen];
			char ur0Path[ur0PLen];
			char vs0Path[vs0PLen];
			char orgPath[orgPLen];
			char orgPathBak[orgPBakLen];
			char pmsg[msgLen];

			snprintf(pmsg, sizeof(char) * msgLen, "Installing custom Live Area: %s...", titleId);
			draw_progress(50, pmsg);

			// Get Live Area override path
			snprintf(ovrdPath, sizeof(char) * ovrdLen, "ux0:theme/%s/extended/%s/contents", themeFolder, titleId);

			if (!exists(ovrdPath)) {
#ifdef THMXD
				sceClibPrintf("applyExTheme: Cannot find override for: %s\nPath: %s\n", titleId, ovrdPath);
#endif
				goto for_continue;
			}

			snprintf(ur0Path, sizeof(char) * ur0PLen, "ur0:appmeta/%s/livearea/contents", titleId);
			snprintf(vs0Path, sizeof(char) * vs0PLen, "vs0:app/%s/sce_sys/livearea/contents", titleId);

#ifdef THMXD
			sceClibPrintf("applyExTheme: ur0 path: %s\nvs0 path: %s\n", ur0Path, vs0Path);
#endif

			if (exists(ur0Path)) {
#ifdef THMXD
				sceClibPrintf("applyExTheme: Found in ur0\n");
#endif
				snprintf(orgPath, sizeof(char) * orgPLen, "ur0:appmeta/%s/livearea/contents", titleId);

			} else if (exists(vs0Path) && !FAST_STRCMP(titleId, VIDEOS) && !FAST_STRCMP(titleId, STORE)) {
#ifdef THMXD
				sceClibPrintf("applyExTheme: Found in vs0\n");
#endif
				snprintf(orgPath, sizeof(char) * orgPLen, "vs0:app/%s/sce_sys/livearea/contents", titleId);

				if (!isVs0Mounted) {
					remount(0x300, 2);
					isVs0Mounted = 1;
				}

			} else {
#ifdef THMXD
				sceClibPrintf("applyExTheme: App data not found: %s\n", titleId);
#endif
				goto for_continue;
			}

			snprintf(orgPathBak, sizeof(char) * orgPBakLen, "%s/active/overrides/%s/contents", THMX_DATA, titleId);

			ret = mkDir(orgPathBak, 0777);
			if (!ret) {
#ifdef THMXD
				sceClibPrintf("applyExTheme: Cannot create org dir: %s\n", orgPath);
#endif
				++errC;
				goto for_continue;
			}

#ifdef THMXD
			sceClibPrintf("applyExTheme: orgpath %s\norgBak %s\n", orgPath, orgPathBak);
#endif
			// Apply the override
			if (FAST_STRCMP(titleId, VIDEOS) || FAST_STRCMP(titleId, STORE)) {
				ret = handleDynLiveArea(ovrdPath, titleId, db, "apply");
#ifdef THMXD
				if (!ret)
					sceClibPrintf("applyExTheme: handleLva error for %s\n", titleId);
#endif
			} else {
				ret = copyDir(orgPath, orgPathBak);
				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot create backup for %s\n", titleId);
#endif
					++errC;
					goto for_continue;
				}

				ret = copyDir(ovrdPath, orgPath);
				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot install override: %s\n", titleId);
#endif
					++errC;
					goto for_continue;
				}
			}
		}

		// Apply custom splash - System Apps ONLY
		if (overrideSplash != NULL && FAST_STRCMP(overrideSplash, "true")) {
			int picLen = tidLen + 26;
			int ovrdPicLen = themeFoldLen + tidLen + 30;
			int picBakLen = thmxDataLen + tidLen + 28;
			char picPath[picLen];
			char ovrdPicPath[ovrdPicLen];
			char picBakPath[picBakLen];

			if (!isVs0Mounted) {
				remount(0x300, 2);
				isVs0Mounted = 1;
			}

			snprintf(picPath, sizeof(char) * picLen, "vs0:app/%s/sce_sys/pic0.png", titleId);
			snprintf(ovrdPicPath, sizeof(char) * ovrdPicLen, "ux0:theme/%s/extended/%s/pic0.png", themeFolder, titleId);
			snprintf(picBakPath, sizeof(char) * picBakLen, "%s/active/overrides/%s/pic0.png", THMX_DATA, titleId);

			if (exists(picPath)) {
				if (!exists(picBakPath)) {
					int picBPLen = thmxDataLen + tidLen + 19;
					char picBakPath[picBPLen];

					snprintf(picBakPath, sizeof(char) * picBPLen, "%s/active/overrides/%s", THMX_DATA, titleId);

					ret = mkDir(picBakPath, 0777);
					if (!ret) {
#ifdef THMXD
						sceClibPrintf("applyExTheme: Cannot create pic backup folder: %s\n", titleId);
#endif
						++errC;
						goto for_continue;
					}
				}

				ret = copyFile(picPath, picBakPath);
				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot backup splash for: %s..Skip...\n", titleId);
#endif
					++errC;
					goto for_continue;
				}

				ret = copyFile(ovrdPicPath, picPath);
				if (!ret) {
#ifdef THMXD
					sceClibPrintf("applyExTheme: Cannot install splash for: %s\n", titleId);
#endif
					++errC;
					goto for_continue;
				}
			}
		}

		// save app data to restore
		fprintf(activeF, "%s %u %s %d %d\n", titleId, titleColorBak, !laStyleBak[0] ? "0":laStyleBak, templateOvrd, laEmpty);

for_continue:
		free(titleId);
		xmlFree(titleColor);
		xmlFree(overrideLva);
		xmlFree(instLva);
		xmlFree(styleOverride);
		xmlFree(overrideSplash);
		xmlFree(instTemplate);
	}

	draw_progress(90, "Closing database...");

	sqlite3_close(db);
	fclose(activeF);
	xmlFreeDoc(doc);

	if (isVs0Mounted)
		remount(0x300, 0);

	if (errC) {
		char msg[34] = {0};

		snprintf(msg, sizeof(msg), "Process completed with %d errors", errC);
		thmx_dialog(2, msg);
	}

	// this is the revert to stock theme, delete data
	if (FAST_STRCMP(themeFolder, "defaultex")) {
		ioRemove(THMX_DATA"/active");
		ioRemove(THMX_DATA"/active.txt");
	}

	return 1;
}

int unsetTheme() {
	const char *sqlUnset = "UPDATE tbl_appinfo_page SET themeFile = NULL, bgColor = 0, texWidth = 0, texHeight = 0, imageWidth = 0, imageHeight = 0 WHERE pageNo >= 0";
	const char *sqlUnsetIcons = "UPDATE tbl_appinfo_icon SET reserved02 = NULL";
	sqlite3 *db;
	int ret;

	ret = sqlite3_open(APPDB, &db);
	if (ret != SQLITE_OK) {
		char msg[40] = {0};

		snprintf(msg, sizeof(msg), "Cannot open the database!\n\nError: %d", ret);
		sqlite3_close(db);
		thmx_dialog(0, msg);
		return 0;
	}

	ret = sql_exec(db, sqlUnsetIcons, "unsetTheme");
	if (ret)
		ret = sql_exec(db, sqlUnset, "unsetTheme");

	if (!ret) {
		sqlite3_close(db);
		thmx_dialog(0, "Cannot unset theme!");
		return 0;
	}

	sqlite3_close(db);

	// 0 -> Tell the system we want to unset the current lockscreen
	ret = sceRegMgrSetKeyInt("/CONFIG/THEME", "wallpaper", 0);
	if (ret < 0)
		thmx_dialog(0, "Cannot unset the lockscreen!");

	ret = sceRegMgrSetKeyStr("/CONFIG/THEME", "current_theme", DEFAULT_THEME, 24);
	if (ret < 0)
		thmx_dialog(0, "Cannot unset the active theme.\nPlease use the Settings app to unset it.");

	ret = ioRemove(THMX_DATA"/theme");
	if (ret < 0)
		return 0;

	return 1;
}

int unsetExTheme() {
	int thmxDataLen = strlen(THMX_DATA);
	sqlite3 *db ;
	FILE *activeF;
	int isVs0Mounted = 0, errC = 0;
	char buff[255] = {0};
	SceUID dfd;
	int ret;

	draw_progress(10, "Preparing to unset...");

	activeF = fopen(THMX_DATA"/active.txt", "r");
	if (activeF == NULL) {
		thmx_dialog(0, "Cannot open backup file!");
		return 0;
	}

	ret = sqlite3_open(APPDB, &db);
	if (ret != SQLITE_OK) {
		char msg[40] = {0};

		snprintf(msg, sizeof(msg), "Cannot open the database!\n\nError: %d", ret);
		sqlite3_close(db);
		fclose(activeF);
		thmx_dialog(0, msg);
		return 0;
	}

	sqlite3_create_function(db, "add_rm_list", 1, SQLITE_UTF8, NULL, &_add_rm_list, NULL, NULL);

	while (activeF != NULL && fgets(buff, 255, activeF) != NULL) {
		int hasTemplate = 0, hasNewLva = 0;
		uint titleClr = 0;
		char titleId[15] = {0};
		char orgStyle[18] = {0};
		char sql[80] = {0};
		char msg[25] = {0};

		sscanf(buff, "%s %u %s %d %d", titleId, &titleClr, orgStyle, &hasTemplate, &hasNewLva);
#ifdef THMXD
		sceClibPrintf("extracted data: %s, %u, %s, %d, %d\n", titleId, titleClr, orgStyle, hasTemplate, hasNewLva);
#endif
		snprintf(msg, sizeof(msg), "Restoring %s...", titleId);
		draw_progress(40, msg);

		if (titleClr) {
			sqlite3_snprintf(sizeof(sql), sql, "UPDATE tbl_livearea SET titleColor = %u WHERE titleId = '%s'", titleClr, titleId);

			ret = sql_exec(db, sql, "unsetExTheme: title color");
			if (!ret)
				++errC;
		}

		if (!FAST_STRCMP(orgStyle, "0")) {
			snprintf(sql, sizeof(sql), "UPDATE tbl_livearea SET style = '%s' WHERE titleId = '%s'", orgStyle, titleId);

			ret = sql_exec(db, sql, "unsetExTheme: org style");
			if (!ret)
				++errC;
		}

		if (hasTemplate) {
			int tlen = thmxDataLen + strlen(titleId) + 42;
			char templatePath[tlen];
			char *lang = getLangString();
			char ur0path[50] = {0};
			char vs0Path[55] = {0};

			if (!isVs0Mounted) {
				remount(0x300, 2);
				isVs0Mounted = 1;
			}

			snprintf(templatePath, sizeof(char) * tlen, "%s/active/overrides/%s/contents/template.xml", THMX_DATA, titleId);

			if (!exists(templatePath)) {
#ifdef THMXD
				sceClibPrintf("Cannot find app resources: %s.\n", templatePath);
#endif
				++errC;

			} else {
				parseAndInstallTemplate(templatePath, titleId, db, lang);

				snprintf(ur0path, sizeof(ur0path), "ur0:appmeta/%s/livearea/contents/custom", titleId);
				snprintf(vs0Path, sizeof(vs0Path), "vs0:app/%s/sce_sys/livearea/contents/custom", titleId);

				if (exists(ur0path)) {
					ret = ioRemove(ur0path);
					if (!ret) {
#ifdef THMXD
						sceClibPrintf("unsetExTheme: template: ur0 delete error\npath: %s\n", ur0path);
#endif
						++errC;
					}
				}
				else if (exists(vs0Path)) {
					ret = ioRemove(vs0Path);
					if (!ret) {
#ifdef THMXD
						sceClibPrintf("unsetExTheme: template: vs0 delete error\npath: %s\n", vs0Path);
#endif
						++errC;
					}
				}
			}
			free(lang);
		}

		if (hasNewLva) {
			char sqlRm[60] = {0};
			char rmPath[50] = {0};

			snprintf(sqlRm, sizeof(sqlRm), "DELETE FROM tbl_livearea WHERE titleId = '%s'", titleId);
			snprintf(rmPath, sizeof(rmPath), "ur0:appmeta/%s/livearea/contents", titleId);

			ret = ioRemove(rmPath);
			if (!ret) {
#ifdef THMXD
				sceClibPrintf("unsetExTheme: hasNewLva: delete error for %s\npath: %s", titleId, rmPath);
#endif
				++errC;
			}

			sql_exec(db, sqlRm, "unsetExTheme: hasNewLva: unset db");
		}
	}

	if (!isVs0Mounted) {
		remount(0x300, 2);
		isVs0Mounted = 1;
	}

	dfd = sceIoDopen(THMX_DATA"/active/overrides");
	if (dfd < 0) {
		sceIoDclose(dfd);
		sqlite3_close(db);

		if (isVs0Mounted)
			remount(0x300, 0);

		thmx_dialog(0, "Cannot open overrides folder!");
		return 0;
	}

	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0 || FAST_STRCMP(dir.d_name, ".") || FAST_STRCMP(dir.d_name, "..") || !SCE_S_ISDIR(dir.d_stat.st_mode))
			continue;

		int dirLen = strlen(dir.d_name);
		int srcLen = dirLen + thmxDataLen + 29;
		int pic0Len= dirLen + 27;
		int lvaPathLen = dirLen + 36;
		int msgLen = dirLen + 15;
		char srcP[srcLen];
		char pic0OvPath[srcLen]; // same no. of chars
		char pic0Path[pic0Len];
		char ur0Path[lvaPathLen];
		char vs0Path[lvaPathLen];
		char msg[msgLen];
		int res;

		snprintf(srcP, sizeof(char) * srcLen, "%s/active/overrides/%s/contents", THMX_DATA, dir.d_name);
		snprintf(pic0OvPath, sizeof(char) * srcLen, "%s/active/overrides/%s/pic0.png", THMX_DATA, dir.d_name);
		snprintf(pic0Path, sizeof(char) * pic0Len, "vs0:app/%s/sce_sys/pic0.png", dir.d_name);
		snprintf(ur0Path, sizeof(char) * lvaPathLen, "ur0:appmeta/%s/livearea/contents", dir.d_name);
		snprintf(vs0Path, sizeof(char) * lvaPathLen, "vs0:app/%s/sce_sys/livearea/contents", dir.d_name);

#ifdef THMXD
		sceClibPrintf("RESTORE APP:\n%s\n%s\n%s\n%s\n%s\n\n", srcP, pic0OvPath, pic0Path, ur0Path, vs0Path);
#endif

		snprintf(msg, sizeof(char) * msgLen, "Restoring %s...", dir.d_name);
		draw_progress(60, msg);

		if (exists(ur0Path)) {
			if (FAST_STRCMP(dir.d_name, VIDEOS) || FAST_STRCMP(dir.d_name, STORE)) {
#ifdef THMXD
				sceClibPrintf("unsetExTheme: lva override: found %s special\n", dir.d_name);
#endif
				res = handleDynLiveArea(srcP, dir.d_name, db, "unset");

			} else {
				res = copyDir(srcP, ur0Path);
#ifdef THMXD
				sceClibPrintf("unsetExTheme: lva override: found ur0: %s\ncopy to %s from %s\n", dir.d_name, ur0Path, srcP);
#endif
			}

		} else if (exists(vs0Path)) {
			res = copyDir(srcP, vs0Path);
#ifdef THMXD
			sceClibPrintf("Found vs0: %s...copy to %s from %s\n", dir.d_name, vs0Path, srcP);
#endif
		}

		if (!res) {
#ifdef THMXD
			sceClibPrintf("Cannot restore resources for: %s", dir.d_name);
#endif
			++errC;
		}

		// Restore background
		if (exists(pic0OvPath)) {
			res = copyFile(pic0OvPath, pic0Path);
			if (!res) {
#ifdef THMXD
				sceClibPrintf("Cannot restore background for: %s", dir.d_name);
#endif
				++errC;
			}
		}
	} while (ret > 0);

	sceIoDclose(dfd);
	sqlite3_close(db);

	if (isVs0Mounted)
		remount(0x300, 0);

	draw_progress(80, "Deleting old files...");

	ret = ioRemove(THMX_DATA"/active");
	if (ret)
		ret = ioRemove(THMX_DATA"/theme");

	if (!ret) {
		thmx_dialog(0, "Cannot delete old files!");
		return 0;
	}

	ret = ioRemove(THMX_DATA"/active.txt");
	if (!ret) {
		thmx_dialog(0, "Cannot delete old files!");
		return 0;
	}

	if (errC) {
		char msg[80] = {0};

		snprintf(msg, sizeof(msg), "Process completed with %d errors\nYou may need to rebuild your database", errC);
		thmx_dialog(2, msg);
	}

	return 1;
}

int doApplyTheme(const char *themeFolder, int isEx) {
	int ret;

	ret = unsetTheme();
#ifdef THMXD
	sceClibPrintf("unset ret %d\n", ret);
#endif
	if (!ret)
		return 0;

	if (exists(THMX_DATA"/active") && exists(THMX_DATA"/active.txt")) {
		ret = unsetExTheme();
		if (!ret)
			return 0;
	}

	ret = applyTheme(themeFolder);
#ifdef THMXD
	sceClibPrintf("apply ret %d\n", ret);
#endif
	if (!ret)
		return 0;

	if (isEx) {
		ret = applyExTheme(themeFolder);
		if (!ret)
			return 0;
	}

	return 1;
}

char *getTitleId(const char *nodeName) {
	int len = strlen(nodeName) + 3;
	size_t retLen = sizeof(char) * 15;
	char extractedName[len];
	char *ret = malloc(retLen);
	int nameSub = 0;

	if (ret == NULL)
		return NULL;

	if (nodeName[1] == '_' && (nodeName[0] == 'm' || nodeName[0] == 'x'))
		nameSub = 2;

	snprintf(extractedName, sizeof(char) * len, "%s", nodeName+nameSub);

	if (FAST_STRCMP(extractedName, "wkpark"))
		snprintf(ret, retLen, "%s", WELCOME_PARK);
	else if (FAST_STRCMP(extractedName, "store"))
		snprintf(ret, retLen, "%s", STORE);
	else if (FAST_STRCMP(extractedName, "browser"))
		snprintf(ret, retLen, "%s", BROWSER);
	else if (FAST_STRCMP(extractedName, "trophies"))
		snprintf(ret, retLen, "%s", TROPHIES);
	else if (FAST_STRCMP(extractedName, "friends"))
		snprintf(ret, retLen, "%s", FRIENDS);
	else if (FAST_STRCMP(extractedName, "messages"))
		snprintf(ret, retLen, "%s", MESSAGES);
	else if (FAST_STRCMP(extractedName, "party"))
		snprintf(ret, retLen, "%s", PARTY);
	else if (FAST_STRCMP(extractedName, "ps4link"))
		snprintf(ret, retLen, "%s", PS4_LINK);
	else if (FAST_STRCMP(extractedName, "parentalctrl"))
		snprintf(ret, retLen, "%s", PARENT_CTRL);
	else if (FAST_STRCMP(extractedName, "videos"))
		snprintf(ret, retLen, "%s", VIDEOS);
	else if (FAST_STRCMP(extractedName, "music"))
		snprintf(ret, retLen, "%s", MUSIC);
	else if (FAST_STRCMP(extractedName, "photos"))
		snprintf(ret, retLen, "%s", PHOTOS);
	else if (FAST_STRCMP(extractedName, "panorama"))
		snprintf(ret, retLen, "%s", PANORAMA);
	else if (FAST_STRCMP(extractedName, "email"))
		snprintf(ret, retLen, "%s", EMAIL);
	else if (FAST_STRCMP(extractedName, "calendar"))
		snprintf(ret, retLen, "%s", CALENDAR);
	else if (FAST_STRCMP(extractedName, "cma"))
		snprintf(ret, retLen, "%s", CMA);
	else if (FAST_STRCMP(extractedName, "near"))
		snprintf(ret, retLen, "%s", NEAR);
	else if (FAST_STRCMP(extractedName, "ps3link"))
		snprintf(ret, retLen, "%s", PS3_LINK);
	else if (FAST_STRCMP(extractedName, "crossctrl"))
		snprintf(ret, retLen, "%s", CROSS_CTRL);
	else if (FAST_STRCMP(extractedName, "settings"))
		snprintf(ret, retLen, "%s", SETTINGS);
	else if (FAST_STRCMP(extractedName, "pkginst"))
		snprintf(ret, retLen, "%s", PKG_INSTALLER);
	else if (FAST_STRCMP(extractedName, "psmdev"))
		snprintf(ret, retLen, "%s", PSM_DEV);
	else if (FAST_STRCMP(extractedName, "psmdevu"))
		snprintf(ret, retLen, "%s", PSM_DEV_U);
	else if (FAST_STRCMP(extractedName, "signup"))
		snprintf(ret, retLen, "%s", SIGNUP);
	else if (FAST_STRCMP(extractedName, "thmx"))
		snprintf(ret, retLen, "%s", THMX_ID);
	else if (FAST_STRCMP(extractedName, "adrenaline"))
		snprintf(ret, retLen, "%s", ADRENALINE);
	else
		snprintf(ret, retLen, "%s", extractedName);

	return ret;
}

int isActiveTheme(const char *themeFolder) {
	int len = strlen(themeFolder) + 12;
	char themeP[len];
	char buff[2000] = {0};
	int ret;

	ret = sceRegMgrGetKeyStr("/CONFIG/THEME", "current_theme", buff, sizeof(buff));
	if (ret < 0)
		return 0;

	snprintf(themeP, sizeof(char) * len, "ux0:/theme/%s", themeFolder);

	return FAST_STRCMP(buff, themeP);
}

int isThemeInstalled(const char *themeFolder) {
	int idLen = strlen(themeFolder) + 12;
	int sqlLen = idLen + 45;
	char themeId[idLen];
	char sql[sqlLen];
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int ret;

	ret = sqlite3_open(APPDB, &db);
	if (ret != SQLITE_OK) {
#ifdef THMXD
		sceClibPrintf("isThemeInstalled(%s) sql_open: %d\n", themeFolder, ret);
#endif
		return 0;
	}

	snprintf(themeId, sizeof(char) * idLen, "ux0:/theme/%s", themeFolder);
	snprintf(sql, sizeof(char) * sqlLen, "SELECT COUNT(*) FROM tbl_theme WHERE id = '%s'", themeId);

	ret = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);
	if (ret != SQLITE_OK) {
#ifdef THMXD
		const char *sqlEr = sqlite3_errmsg(db);

		sceClibPrintf("isThemeInstalled(%s) sql_prepare: %d\n", themeFolder, sqlEr);
#endif
		sqlite3_close(db);
		return 0;
	}

	if (sqlite3_step(stmt) == SQLITE_ROW)
		ret = sqlite3_column_int(stmt, 0);

	sqlite3_finalize(stmt);
	sqlite3_close(db);

	return (ret > 0);
}

int handleDynLiveArea(const char *themeOvrdPath, const char *titleId, sqlite3 *db, const char *mode) {
	const char *imgs[] = {"dyn_bg.png", "dyn_startup.png"};
	int tidLen = strlen(titleId);
	int sqlLen = tidLen + 82;
	int srcLen = strlen(themeOvrdPath) + 18;
	int dstLen = tidLen + 45;
	int dstBLen = tidLen + strlen(THMX_DATA) + 46;
	char sql[sqlLen];
	sqlite3_stmt *stmt;
	int i = 0;
	int ret;

	snprintf(sql, sizeof(char) * sqlLen, "SELECT background_image, gate_startupImage FROM tbl_livearea WHERE titleId = '%s'", titleId);

	ret = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);
	if (ret != SQLITE_OK) {
#ifdef THMXD
		const char *sqlEr = sqlite3_errmsg(db);

		sceClibPrintf("handleDynLva: Cannot execute query: ID: %s\nErr: %s", titleId, sqlEr);
#endif
		return 0;
	}

	ret = sqlite3_step(stmt);
	if (ret != SQLITE_ROW)
		return 0;

	while (i < 2) {
		char src[srcLen];
		char dst[dstLen];

		snprintf(src, sizeof(char) * srcLen, "%s/%s", themeOvrdPath, imgs[i]);
		sqlite3_snprintf(sizeof(char) * dstLen, dst, "ur0:appmeta/%s/livearea/contents/%s", titleId, sqlite3_column_text(stmt, i));

#ifdef THMXD
		sceClibPrintf("handleDynLva: Src: %s\nDst: %s\n", src, dst);
#endif

		if (!exists(src) || !exists(dst)) {
#ifdef THMXD
			sceClibPrintf("handleDynLva: Cannot find: %s or %s\n", src, dst);
#endif
			++i;
			continue;
		}

		if (FAST_STRCMP(mode, "apply")) {
			char dstB[dstBLen];

			snprintf(dstB, sizeof(char) * dstBLen, "%s/active/overrides/%s/contents/%s", THMX_DATA, titleId, imgs[i]);

			ret = copyFile(dst, dstB);
			if (!ret) {
				sqlite3_finalize(stmt);
#ifdef THMXD
			sceClibPrintf("handleDynLva: apply copy error: DstB: %s\n", dstB);
#endif
				return 0;
			}
		}

		ret = copyFile(src, dst);
		if (!ret) {
			sqlite3_finalize(stmt);
#ifdef THMXD
			sceClibPrintf("handleDynLva: copy error: src: %s\ndst: %s\n", src, dst);
#endif
			return 0;
		}
		++i;
	}

	sqlite3_finalize(stmt);

	return 1;
}

int exify(const char *themeFolder) {
	const char *xmlFile = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<theme format=\"thmx\" version=\"1.0\">\n\n</theme>";
	int len = strlen(themeFolder) + 24;
	char themePath[len];
	char themeXmlPath[len];
	int errC = 0, prog = 0, progInc = 0;
	SceUID dfd;
	FILE *logF, *themeXml;
	int ret;

	draw_progress(0, "Starting...");

	snprintf(themePath, sizeof(char) * len, "ux0:theme/%s/extended", themeFolder);
	snprintf(themeXmlPath, sizeof(char) * len, "ux0:theme/%s/themeEx.xml", themeFolder);

	draw_progress(5, "Creating Folder...");

	ret = mkDir(themePath, 0777);
	if (!ret) {
		thmx_dialog(0, "EXify: Cannot create folders!");
		return 0;
	}

	draw_progress(10, "Opening ur0:appmeta...");

	dfd = sceIoDopen("ur0:appmeta");
	if (dfd < 0) {
		thmx_dialog(0, "EXify: Cannot open ur0:appmeta!");
		return 0;
	}

	logF = fopen(THMX_DATA"/exify_log.txt", "a");
	if (logF == NULL) {
		sceIoDclose(dfd);
		thmx_dialog(0, "EXify: Cannot create log file!");
		return 0;
	}

	fprintf(logF, "EXify %s\n\n", themeFolder);

	prog = 10;
	progInc = floor(countFolders("ur0:appmeta") / 40);
	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0 || FAST_STRCMP(dir.d_name, ".") || FAST_STRCMP(dir.d_name, "..") || !SCE_S_ISDIR(dir.d_stat.st_mode))
			continue;

		int dirLen = strlen(dir.d_name);
		int srcLen = dirLen + 32;
		int dstLen = dirLen + strlen(themePath) + 11;
		int msgLen = dirLen + 12;
		char srcP[srcLen];
		char dstP[dstLen];
		char msg[msgLen];
		int res;

		snprintf(srcP, sizeof(char) * srcLen, "ur0:appmeta/%s/livearea/contents", dir.d_name);
		snprintf(dstP, sizeof(char) * dstLen, "%s/%s/contents", themePath, dir.d_name);
		snprintf(msg, sizeof(char) * msgLen, "Copying %s...", dir.d_name);

		if (!exists(srcP))
			continue;

		res = mkDir(dstP, 0777);
		if (!res) {
			++errC;
			fprintf(logF, "ur0:appmeta/%s... mkdir error\n", dir.d_name);
			continue;
		}

		res = copyDir(srcP, dstP);
		if (!res) {
			++errC;
			fprintf(logF, "ur0:appmeta/%s... copy error\n", dir.d_name);
		}

		prog += progInc;
		draw_progress(prog, msg);
	} while (ret > 0);

	sceIoDclose(dfd);

	draw_progress(50, "Opening vs0:app...");

	dfd = sceIoDopen("vs0:app");
	if (dfd < 0) {
		fclose(logF);
		thmx_dialog(0, "EXify: Cannot open vs0:app!");
		return 0;
	}

	prog = 50;
	progInc = floor(countFolders("vs0:app") / 40);
	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0 || FAST_STRCMP(dir.d_name, ".") || FAST_STRCMP(dir.d_name, "..") ||
				FAST_STRCMP(dir.d_name, VIDEOS) || FAST_STRCMP(dir.d_name, STORE) || !SCE_S_ISDIR(dir.d_stat.st_mode))
			continue;

		int dirLen = strlen(dir.d_name);
		int srcLen = dirLen + 35;
		int dstLen = dirLen + strlen(themePath) + 11;
		int msgLen = dirLen + 12;
		char srcP[srcLen];
		char dstP[dstLen];
		char msg[msgLen];
		int res;

		snprintf(srcP, sizeof(char) * srcLen, "vs0:app/%s/sce_sys/livearea/contents", dir.d_name);
		snprintf(dstP, sizeof(char) * dstLen, "%s/%s/contents", themePath, dir.d_name);
		snprintf(msg, sizeof(char) * msgLen, "Copying %s...", dir.d_name);

		if (!exists(srcP))
			continue;

		res = mkDir(dstP, 0777);
		if (!res) {
			++errC;
			fprintf(logF, "vs0:app/%s... mkdir error\n", dir.d_name);
			continue;
		}

		res = copyDir(srcP, dstP);
		if (!res) {
			++errC;
			fprintf(logF, "vs0:app/%s... copy error\n", dir.d_name);
			continue;
		}

		prog += progInc;
		draw_progress(prog, msg);
	} while (ret > 0);

	sceIoDclose(dfd);
	fclose(logF);

	draw_progress(90, "Writing themeEx.xml...");

	themeXml = fopen(themeXmlPath, "w");
	if (themeXml == NULL) {
		thmx_dialog(0, "EXify: Cannot create themeEx.xml!");
		return 0;
	}

	ret = fputs(xmlFile, themeXml);
	if (ret < 0) {
		fclose(themeXml);
		thmx_dialog(0, "EXify: Xml file is not writable!");
		return 0;
	}

	fclose(themeXml);
	draw_progress(100, "Done.");

	if (errC) {
		int len = strlen(THMX_DATA) + 72;
		char msg[len];

		snprintf(msg, sizeof(char) * len, "Process completed with %d errors\n\nLogs written to:\n%s/exify_log.txt", errC, THMX_DATA);
		thmx_dialog(2, msg);
	}

	return 1;
}

char *getLangString() {
	size_t retLen = sizeof(char) * 6;
	char *ret = malloc(retLen);

	if (ret == NULL) {
		thmx_dialog(0, "getLang: cannot allocate memory!");
		sceKernelExitProcess(1);
	}

	switch (resources.sysLanguage) {
		case SCE_SYSTEM_PARAM_LANG_JAPANESE:
			snprintf(ret, retLen, "ja");
			break;
		case SCE_SYSTEM_PARAM_LANG_ENGLISH_US:
			snprintf(ret, retLen, "en");
			break;
		case SCE_SYSTEM_PARAM_LANG_FRENCH:
			snprintf(ret, retLen, "fr");
			break;
		case SCE_SYSTEM_PARAM_LANG_SPANISH:
			snprintf(ret, retLen, "es");
			break;
		case SCE_SYSTEM_PARAM_LANG_GERMAN:
			snprintf(ret, retLen, "de");
			break;
		case SCE_SYSTEM_PARAM_LANG_ITALIAN:
			snprintf(ret, retLen, "it");
			break;
		case SCE_SYSTEM_PARAM_LANG_DUTCH:
			snprintf(ret, retLen, "nl");
			break;
		case SCE_SYSTEM_PARAM_LANG_PORTUGUESE_PT:
			snprintf(ret, retLen, "pt");
			break;
		case SCE_SYSTEM_PARAM_LANG_RUSSIAN:
			snprintf(ret, retLen, "ru");
			break;
		case SCE_SYSTEM_PARAM_LANG_KOREAN:
			snprintf(ret, retLen, "ko");
			break;
		case SCE_SYSTEM_PARAM_LANG_CHINESE_T:
			snprintf(ret, retLen, "ch");
			break;
		case SCE_SYSTEM_PARAM_LANG_CHINESE_S:
			snprintf(ret, retLen, "zh");
			break;
		case SCE_SYSTEM_PARAM_LANG_FINNISH:
			snprintf(ret, retLen, "fi");
			break;
		case SCE_SYSTEM_PARAM_LANG_SWEDISH:
			snprintf(ret, retLen, "sv");
			break;
		case SCE_SYSTEM_PARAM_LANG_DANISH:
			snprintf(ret, retLen, "da");
			break;
		case SCE_SYSTEM_PARAM_LANG_NORWEGIAN:
			snprintf(ret, retLen, "no");
			break;
		case SCE_SYSTEM_PARAM_LANG_POLISH:
			snprintf(ret, retLen, "pl");
			break;
		case SCE_SYSTEM_PARAM_LANG_PORTUGUESE_BR:
			snprintf(ret, retLen, "pt-br");
			break;
		case SCE_SYSTEM_PARAM_LANG_ENGLISH_GB:
			snprintf(ret, retLen, "en-gb");
			break;
		case SCE_SYSTEM_PARAM_LANG_TURKISH:
			snprintf(ret, retLen, "tr");
			break;
		default:
			snprintf(ret, retLen, "en");
			break;
	}

	return ret;
}

int sql_exec(sqlite3 *db, const char *query, const char *funcToLog) {
	char *error;
	int ret;

	ret = sqlite3_exec(db, query, NULL, NULL, &error);
	if (ret != SQLITE_OK) {
#ifdef THMXD
		sceClibPrintf("%s: sql_exec: error: %s\n", funcToLog, error);
#endif
		sqlite3_free(error);
		return 0;
	}

	return 1;
}

void _add_rm_list(sqlite3_context *context, int argc, sqlite3_value **argv) {
	int ret = ioRemove(sqlite3_value_text(argv[0]));

	return;
}

void themesListFree(char **themesList, int count) {
	for (int i=0; i<count; ++i)
		free(themesList[i]);

	free(themesList);
}

// Credits TheOfficialFlow
int vshIoMount(int id, const char *path, int permission, int a4, int a5, int a6) {
  uint32_t buf[3];

  buf[0] = a4;
  buf[1] = a5;
  buf[2] = a6;

  return _vshIoMount(id, path, permission, buf);
}

// Credits TheOfficialFlow
int remount(int id, int perms) {
  vshIoUmount(id, 0, 0, 0);
  vshIoUmount(id, 1, 0, 0);

  return vshIoMount(id, NULL, perms, 0, 0, 0);
}