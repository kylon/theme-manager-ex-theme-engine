#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#include <psp2/io/stat.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/dirent.h>

#ifdef THMXD
#include <psp2/kernel/clib.h>
#endif

#include "include/file.h"

int exists(const char *path) {
	SceIoStat stat = {0};

	return sceIoGetstat(path, &stat) >= 0;
}

int touchFile(const char *path) {
	int ret;

	ret = sceIoOpen(path, SCE_O_WRONLY | SCE_O_CREAT, 0777);
	sceIoClose(ret);

	return (ret >= 0);
}

int isDir(const char *path) {
	SceIoStat stat = {0};

	if (sceIoGetstat(path, &stat) < 0)
		return 0;

	return SCE_S_ISDIR(stat.st_mode);
}

int mkDir(const char *path, int mode) {
	if (isDir(path))
		return 1;

	int len = strlen(path);
	char npath[len];
	int start = 0;
	int ret;

	memset(npath, 0, len);

	for (int i=0; i<len; ++i) {
		npath[i] = path[i];

		if (!start < 2 && path[i] == ':')
			start = 1;

		if (path[i] != '/' || !start || isDir(npath))
			continue;

		ret = sceIoMkdir(npath, mode);
		if (ret < 0)
			return 0;
	}

	ret = sceIoMkdir(path, mode);
	if (ret < 0)
		return 0;

	return 1;
}

int ioRemove(const char *path) {
	if (!exists(path))
		return 1;

	SceUID dfd = sceIoDopen(path);
	int res = 0;

#ifdef THMXD
	sceClibPrintf("call ioRemove(%s)\n", path);
#endif
	if (dfd < 0) { // not a folder, file?
		if (sceIoRemove(path) >= 0) // file
			return 1;

#ifdef THMXD
		sceClibPrintf("E: not a folder nor a file!\n");
#endif
		return 0;
	}

	do {
		SceIoDirent dir;
		char *newPath = NULL;
		size_t pLen = 0;
		int ret;

		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res <= 0 || !strcmp(dir.d_name, ".") || !strcmp(dir.d_name, ".."))
			continue;

#ifdef THMXD
		sceClibPrintf("current dir: %s\n", dir.d_name);
#endif

		pLen = sizeof(char) * (strlen(path) + strlen(dir.d_name) + 2);
		newPath = malloc(pLen);

		snprintf(newPath, pLen, "%s/%s", path, dir.d_name);
#ifdef THMXD
		sceClibPrintf("newPath = %s\n", newPath);
#endif
		if (SCE_S_ISDIR(dir.d_stat.st_mode))
			ret = ioRemove(newPath) == 0 ? -1:1;
		else
			ret = sceIoRemove(newPath);

		free(newPath);
		if (ret < 0) {
			sceIoDclose(dfd);
			return 0;
		}
	} while (res > 0);

	sceIoDclose(dfd);

	if (sceIoRmdir(path) < 0)
		return 0;

	return 1;
}

int copyFile(const char *src, const char *dest) {
	if (!strcasecmp(src, dest))
		return 1;
	else if (!exists(src))
		return 0;

	void *buf = memalign(4096, 128 * 1024);
	int fsrc = sceIoOpen(src, SCE_O_RDONLY, 0);
	int fdst  = sceIoOpen(dest, SCE_O_WRONLY | SCE_O_CREAT, 0777);
	int read = 0, written = 0;
	SceIoStat stat;

	if (fsrc < 0 || fdst < 0) {
#ifdef THMXD
		sceClibPrintf("copyfile fail: src: %s, dst: %s\n", src, dest);
#endif
		free(buf);
		sceIoClose(fsrc);
		sceIoClose(fdst);
		return 0;
	}

	while (1) {
		read = sceIoRead(fsrc, buf, 128 * 1024);
		if (read < 0) {
			free(buf);
			sceIoClose(fsrc);
			sceIoClose(fdst);
			sceIoRemove(dest);
#ifdef THMXD
			sceClibPrintf("copyFile: read error\nsrc: %s\ndst: %s\n", src, dest);
#endif
			return 0;

		} else if (!read) {
			break;
		}

		written = sceIoWrite(fdst, buf, read);
		if (written < 0) {
			free(buf);
			sceIoClose(fsrc);
			sceIoClose(fdst);
			sceIoRemove(dest);
#ifdef THMXD
			sceClibPrintf("copyFile: write error\nsrc: %s\ndst: %s\n", src, dest);
#endif
			return 0;
		}
	}

	free(buf);

	// Inherit file stat
  	memset(&stat, 0, sizeof(SceIoStat));
  	sceIoGetstatByFd(fsrc, &stat);
	sceIoChstatByFd(fdst, &stat, 0x3B);

	sceIoClose(fsrc);
	sceIoClose(fdst);
#ifdef THMXD
	sceClibPrintf("copyFile ok: src: %s, dst: %s\n", src, dest);
#endif

	return 1;
}

int copyDir(const char *src, const char *dest) {
	if (!strcasecmp(src, dest))
		return 1;

	SceUID dfd = sceIoDopen(src);
	SceIoStat stat;
	int res = 0, ret = 0;

	if (dfd < 0) // not a folder?
		return copyFile(src, dest);

    memset(&stat, 0, sizeof(SceIoStat));
    sceIoGetstatByFd(dfd, &stat);

    stat.st_mode |= SCE_S_IWUSR;

    ret = sceIoMkdir(dest, stat.st_mode & 0xFFF);
    if (ret < 0 && ret != SCE_ERROR_ERRNO_EEXIST) {
    	sceIoDclose(dfd);
    	return 0;
    }

    if (ret == SCE_ERROR_ERRNO_EEXIST)
		sceIoChstat(dest, &stat, 0x3B);

#ifdef THMXD
	sceClibPrintf("copydir: start\n");
#endif

	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res <= 0 || !strcmp(dir.d_name, ".") || !strcmp(dir.d_name, ".."))
			continue;

		int dPathLen = strlen(dir.d_name);
		int srcLen = strlen(src) + dPathLen + 4;
		int dstLen = strlen(dest) + dPathLen + 4;
		char *new_src = malloc(srcLen);
		char *new_dst = malloc(dstLen);

		snprintf(new_src, srcLen, "%s/%s", src, dir.d_name);
		snprintf(new_dst, dstLen, "%s/%s", dest, dir.d_name);
#ifdef THMXD
		sceClibPrintf("copydir: cur item: %s\nnewsrc: %s\nnewdest: %s\n", dir.d_name, new_src, new_dst);
#endif

		if (SCE_S_ISDIR(dir.d_stat.st_mode))
			ret = copyDir(new_src, new_dst);
		else
			ret = copyFile(new_src, new_dst);

		free(new_src);
		free(new_dst);

		if (!ret) {
			sceIoDclose(dfd);
#ifdef THMXD
			sceClibPrintf("copydir: copy fail\n");
#endif
			return 0;
		}
	} while (res > 0);

	sceIoDclose(dfd);

#ifdef THMXD
	sceClibPrintf("copydir: ok:\n%s\n%s\n", src, dest);
#endif

	return 1;
}

int countFolders(const char *path) {
	SceUID dfd = sceIoDopen(path);
	int i = 0;
	int ret;

	if (dfd < 0)
		return -1;

	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		ret = sceIoDread(dfd, &dir);
		if (ret <= 0 || !strcmp(dir.d_name, ".") || !strcmp(dir.d_name, "..") || !SCE_S_ISDIR(dir.d_stat.st_mode))
			continue;

		++i;
	} while(ret > 0);

	return i;
}
