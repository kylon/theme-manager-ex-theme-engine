#include "include/template_utils.h"

char *getAttributesString(xmlNode *node) {
	size_t len = sizeof(char) * 500;
	char *attrStr = malloc(len);

	if (attrStr == NULL)
		return NULL;

	attrStr[0] = 0;

	// Common
	xmlChar *align = xmlGetProp(node, "align");
	xmlChar *valign = xmlGetProp(node, "valign");
	xmlChar *width = xmlGetProp(node, "width");
	xmlChar *marginLeft = xmlGetProp(node, "margin-left");
	xmlChar *marginTop = xmlGetProp(node, "margin-top");
	xmlChar *x = xmlGetProp(node, "x");
	xmlChar *y = xmlGetProp(node, "y");

	// The first
	if (align != NULL)
		snprintf(attrStr, len, "align=\"%s\"", align);

	// text only
	if (FAST_STRCMP(node->name, "text")) {
		xmlChar *ellipsis = xmlGetProp(node, "ellipsis");
		xmlChar *lineAlign = xmlGetProp(node, "line-align");
		xmlChar *lineBreak = xmlGetProp(node, "line-break");
		xmlChar *origin = xmlGetProp(node, "origin");
		xmlChar *preBr = xmlGetProp(node, "pre-br");
		xmlChar *wordScroll = xmlGetProp(node, "word-scroll");
		xmlChar *wordWrap = xmlGetProp(node, "word-wrap");
		xmlChar *lineSpace = xmlGetProp(node, "line-space");
		xmlChar *textAlign = xmlGetProp(node, "text-align");
		xmlChar *textVAlign = xmlGetProp(node, "text-valign");

		snprintf(attrStr, len,
			"%s ellipsis=\"%s\" line-align=\"%s\" line-break=\"%s\" origin=\"%s\" pre-br=\"%s\" word-scroll=\"%s\" word-wrap=\"%s\" line-space=\"%s\" text-align=\"%s\" text-valign=\"%s\"",
			attrStr,
			ellipsis != NULL ? ellipsis:(xmlChar *)"off",
			lineAlign != NULL ? lineAlign:(xmlChar *)"left",
			lineBreak != NULL ? lineBreak:(xmlChar *)"on",
			origin != NULL ? origin:(xmlChar *)"background",
			preBr != NULL ? preBr:(xmlChar *)"on",
			wordScroll != NULL ? wordScroll:(xmlChar *)"off",
			wordWrap != NULL ? wordWrap:(xmlChar *)"on",
			lineSpace != NULL ? lineSpace:(xmlChar *)"",
			textAlign != NULL ? textAlign:(align != NULL ? align:(xmlChar *)""),
			textVAlign != NULL ? textVAlign:(xmlChar *)"center"
		);

		xmlFree(ellipsis);
		xmlFree(lineAlign);
		xmlFree(lineBreak);
		xmlFree(origin);
		xmlFree(preBr);
		xmlFree(wordScroll);
		xmlFree(wordWrap);
		xmlFree(lineSpace);
		xmlFree(textAlign);
		xmlFree(textVAlign);

	} else if (FAST_STRCMP(node->name, "str")) { // str only
		xmlChar *bold = xmlGetProp(node, "bold");
		xmlChar *color = xmlGetProp(node, "color");
		xmlChar *emboss = xmlGetProp(node, "emboss");
		xmlChar *oblique = xmlGetProp(node, "oblique");
		xmlChar *shadow = xmlGetProp(node, "shadow");
		xmlChar *size = xmlGetProp(node, "size");
		xmlChar *underline = xmlGetProp(node, "underline");

		snprintf(attrStr, len, "%s bold=\"%s\" color=\"%s\" emboss=\"%s\" oblique=\"%s\" shadow=\"%s\" underline=\"%s\"",
			attrStr,
			bold != NULL ? bold:(xmlChar *)"off",
			color != NULL ? color:(xmlChar *)"",
			emboss != NULL ? emboss:(xmlChar *)"off",
			oblique != NULL ? oblique:(xmlChar *)"off",
			shadow != NULL ? shadow:(xmlChar *)"off",
			underline != NULL ? underline:(xmlChar *)"off"
		);

		if (size != NULL)
			snprintf(attrStr, len, "%s size=\"%.6f\"", attrStr, (float)atoi(size));

		xmlFree(bold);
		xmlFree(color);
		xmlFree(emboss);
		xmlFree(oblique);
		xmlFree(shadow);
		xmlFree(size);
		xmlFree(underline);

	} else if (FAST_STRCMP(node->name, "liveitem")) { // liveitem only
		xmlChar *from = xmlGetProp(node, "from"); // TIME
		xmlChar *until = xmlGetProp(node, "until"); // TIME

		if (from != NULL)
			snprintf(attrStr, len, "%s from=\"%s\"", attrStr, from);

		if (until != NULL)
			snprintf(attrStr, len, "%s until=\"%s\"", attrStr, until);

		xmlFree(from);
		xmlFree(until);
	}

	// Other common
	if (valign != NULL)
		snprintf(attrStr, len, "%s valign=\"%s\"", attrStr, valign);

	if (width != NULL)
		snprintf(attrStr, len, "%s width=\"%s\"", attrStr, width);

	if (marginLeft != NULL)
		snprintf(attrStr, len, "%s margin-left=\"%s\"", attrStr, marginLeft);

	if (marginTop != NULL)
		snprintf(attrStr, len, "%s margin-top=\"%s\"", attrStr, marginTop);

	if (x != NULL)
		snprintf(attrStr, len, "%s x=\"%d\"", attrStr, atoi(x));

	if (y != NULL)
		snprintf(attrStr, len, "%s y=\"%d\"", attrStr, atoi(y));

	xmlFree(align);
	xmlFree(valign);
	xmlFree(width);
	xmlFree(marginLeft);
	xmlFree(marginTop);
	xmlFree(x);
	xmlFree(y);

	return attrStr;
}

/*
 * Parse and install a template.xml
 *
 * Support for complex templates is partial.
 * Support for complex multi-lang templates is partial.
 * Support for ps1 templates is untested.
 * Items flip is not supported.
 * Items > 0 are not supported.
 * userdata is not supported.
 *
 * <model></model> tag support is partial.
 * (Set 'vita' to show this element on vita, remove the tag to show the element on vita TV)
 */
int parseAndInstallTemplate(const char *templatePath, const char *titleId, sqlite3 *db, const char *lang) {
	xmlDoc *doc = xmlReadFile(templatePath, NULL, 0);

#ifdef THMXD
	sceClibPrintf("call parseAndInstallTemplate(%s, %s, db, %s)\n", templatePath, titleId, lang);
#endif

	if (doc == NULL) {
#ifdef THMXD
		sceClibPrintf("parseAndInstallTemplate: cannot open xml doc\n");
#endif
		return 0;
	}

	xmlNode *root = xmlDocGetRootElement(doc);
	xmlNode *cur_node = root->children;
	char sqlDeleteOld[256] = {0};
	int ret;

	snprintf(sqlDeleteOld, sizeof(sqlDeleteOld), "DELETE FROM tbl_livearea_frame WHERE titleId = '%s'", titleId);

	ret = sql_exec(db, sqlDeleteOld, "parseAndInstallTemplate: delete");
	if (!ret) {
		xmlFreeDoc(doc);
		return 0;
	}

	for (; cur_node; cur_node = cur_node->next) {
		if (cur_node->type != XML_ELEMENT_NODE || !FAST_STRCMP(cur_node->name, "frame") || cur_node->children == NULL || cur_node->children->next == NULL)
			continue;

		char frameToDb[1024] = {0}; // todo check
		char defElem[1024] = {0};
		char bg0[256] = {0};
		char img0[256] = {0};
		int isDefEl = 0;
		int isFirst = 0;
		int isEmpty = 0;
		xmlChar *frameId = xmlGetProp(cur_node, "id");
		xmlChar *frameFlip = xmlGetProp(cur_node, "autoflip");
		xmlChar *frameMulti = xmlGetProp(cur_node, "multi");
		int frameAutoFlip = frameFlip == NULL ? 0:atoi(frameFlip);
		int fMulti = frameMulti == NULL || FAST_STRCMP(frameMulti, "o") ? 0:atoi(frameMulti);
		xmlNode *tmp = cur_node->children;

		xmlFree(frameFlip);
		xmlFree(frameMulti);

		while (tmp != NULL) {
			if (cur_node->type != XML_ELEMENT_NODE || !FAST_STRCMP(tmp->name, "liveitem")) {
				tmp = tmp->next;
				continue;
			}

			xmlChar *defaultAttr = xmlGetProp(tmp, "default");
			xmlNode *lvitem = tmp->children;

			isDefEl = defaultAttr != NULL && FAST_STRCMP(defaultAttr, "on") ? 1:0;
			xmlFree(defaultAttr);

			if (!isFirst) {
				xmlChar *liveItmAtrr = getAttributesString(tmp);

				snprintf(frameToDb, sizeof(frameToDb), "<liveitem %s>", liveItmAtrr);
				xmlFree(liveItmAtrr);
				isFirst = 1;
			}

			if (lvitem == NULL || lvitem->next == NULL) { // empty live item for future usage
				isEmpty = 1;
				break;
			}

			while (lvitem != NULL) {
				if (lvitem->type != XML_ELEMENT_NODE) {
					lvitem = lvitem->next;
					continue;
				}

				xmlChar *nCont = xmlNodeGetContent(lvitem);

				if (FAST_STRCMP(lvitem->name, "lang") && !FAST_STRCMP(nCont, lang) ||
					FAST_STRCMP(lvitem->name, "cntry") && !FAST_STRCMP(nCont, lang) ||
					FAST_STRCMP(lvitem->name, "model") && !FAST_STRCMP(nCont, "vita") ||
					FAST_STRCMP(lvitem->name, "exclude-lang") && FAST_STRCMP(nCont, lang)) {
					xmlFree(nCont);
					break;
				}
				xmlFree(nCont);

				if (FAST_STRCMP(lvitem->name, "text")) {
					xmlNode *text = lvitem->children;
					char *txAttr = getAttributesString(lvitem);

					if (isDefEl)
						snprintf(defElem, sizeof(defElem), "%s <text %s>", defElem, txAttr);
					else
						snprintf(frameToDb, sizeof(frameToDb), "%s <text %s>", frameToDb, txAttr);

					free(txAttr);

					while (text != NULL) {
						if (lvitem->type != XML_ELEMENT_NODE) {
							text = text->next;
							continue;
						}

						if (FAST_STRCMP(text->name, "str")) {
							char *strAttr = getAttributesString(text);
							xmlChar *cont = xmlNodeGetContent(text);

							if (isDefEl)
								snprintf(defElem, sizeof(defElem), "%s <str %s>%s</str>", defElem, strAttr, cont);
							else
								snprintf(frameToDb, sizeof(frameToDb), "%s <str %s>%s</str>", frameToDb, strAttr, cont);

							free(strAttr);
							xmlFree(cont);

						} else if (FAST_STRCMP(text->name, "font")) {
							char *fontAttr = getAttributesString(text);
							xmlChar *cont = xmlNodeGetContent(text);

							if (isDefEl)
								snprintf(defElem, sizeof(defElem), "%s <font %s>%s</font>", defElem, fontAttr, cont);
							else
								snprintf(frameToDb, sizeof(frameToDb), "%s <font %s>%s</font>", frameToDb, fontAttr, cont);

							free(fontAttr);
							xmlFree(cont);
						}
						text = text->next;
					}

					if (isDefEl)
						snprintf(defElem, sizeof(defElem), "%s </text>", defElem);
					else
						snprintf(frameToDb, sizeof(frameToDb), "%s </text>", frameToDb);

				} else if (!FAST_STRCMP(lvitem->name, "lang") && !FAST_STRCMP(lvitem->name, "model") && !FAST_STRCMP(lvitem->name, "exclude-lang")) {
					// Other elements
					char *attr = getAttributesString(lvitem);
					xmlChar *cont = xmlNodeGetContent(lvitem);

					if (FAST_STRCMP(lvitem->name, "background")) {
						xmlChar *bgImage = xmlNodeGetContent(lvitem);

						snprintf(bg0, sizeof(bg0), "%s", bgImage);
						xmlFree(bgImage);
					}

					if (FAST_STRCMP(lvitem->name, "image")) {
						xmlChar *image0 = xmlNodeGetContent(lvitem);

						snprintf(img0, sizeof(img0), "%s", image0);
						xmlFree(image0);
					}

					snprintf(frameToDb, sizeof(frameToDb), "%s <%s %s>%s</%s>", frameToDb, lvitem->name, attr, cont, lvitem->name);

					free(attr);
					xmlFree(cont);
				}
				lvitem = lvitem->next;
			}
			tmp = tmp->next;
		}

		// No value, check default
		if (frameToDb[0] == 0 && defElem[0] != 0)
			sqlite3_snprintf(sizeof(frameToDb), frameToDb, "%q", defElem);

		if (frameToDb[0] != 0) {
			int frameIdLen = xmlStrlen(frameId);
			int titleIdLen = strlen(titleId);

			sqlite3_snprintf(sizeof(frameToDb), frameToDb, "%q</liveitem>", frameToDb);

			int len = strlen(frameToDb) + frameIdLen + titleIdLen + 750;
			char sql[len];

			sqlite3_snprintf(sizeof(char) * len, sql,
					"INSERT INTO tbl_livearea_frame (userdata, item9_img_path, item9_bg_path, item9_xml, item8_img_path, item8_bg_path, item8_xml, item7_img_path, item7_bg_path, item7_xml, item6_img_path, item6_bg_path, item6_xml, item5_img_path, item5_bg_path, item5_xml, item4_img_path, item4_bg_path, item4_xml, item3_img_path, item3_bg_path, item3_xml, item2_img_path, item2_bg_path, item2_xml, item1_img_path, item1_bg_path, item1_xml, item0_img_path, item0_bg_path, item0_xml, frameRev, frameAutoFlip, frameMulti, type, frameId, titleId ) VALUES ( NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '%q', 0, %d, %d, 1, '%q', '%s')",
					frameToDb, frameAutoFlip, fMulti, frameId, titleId);

			sql_exec(db, sql, "parseAndInstallTemplaet: frametodb!=0");

			if (bg0[0] != 0) {
				int len = strlen(bg0) + titleIdLen + frameIdLen + 90;
				char sql[len];

				sqlite3_snprintf(sizeof(char) * len, sql,
					"UPDATE tbl_livearea_frame SET item0_bg_path = '%q' WHERE titleId = '%s' AND frameId = '%q'",
					bg0, titleId, frameId);

				sql_exec(db, sql, "parseAndInstallTemplaet: bg0!=0");
			}

			if (img0[0] != 0) {
				int len = strlen(img0) + titleIdLen + frameIdLen + 95;
				char sql[len];

				sqlite3_snprintf(sizeof(char) * len, sql,
					"UPDATE tbl_livearea_frame SET item0_img_path = '%q' WHERE titleId = '%s' AND frameId = '%q'",
					img0, titleId, frameId);

				sql_exec(db, sql, "parseAndInstallTemplaet: img0!=0");
			}
		}
		xmlFree(frameId);
	}

	xmlFreeDoc(doc);

	return 1;
}
