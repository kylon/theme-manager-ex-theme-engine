#include <psp2/power.h>

#include "include/gfx_utils.h"

int getBatteryRectX(int percentage) {
	int rectX = 900;

	if (percentage <= 20)
		rectX = 930;
	else if (percentage <= 50)
		rectX = 920;
	else if (percentage <= 70)
		rectX = 910;

	return rectX;
}

int getBatWidth(int percentage) {
	int width = 45;

	if (percentage <= 20)
		width = 15;
	else if (percentage <= 50)
		width = 25;
	else if (percentage <= 70)
		width = 35;

	return width;
}

uint getBatColor(int percentage) {
	uint color = RGBA8(30,179,45,255);

	if (scePowerIsBatteryCharging())
		color = RGBA8(255, 145, 7, 255);
	else if (percentage <= 20)
		color = RGBA8(255, 41, 41, 255);

	return color;
}

void setTexScaleAndCoordCommon(uint textureWidth, float *xLk, float *xHm, float *scaleX, float *scaleY) {
	if (textureWidth > 900) {
		*xLk = 80;
		*xHm = 495;
		*scaleX = *scaleY = 0.400f;
	} else if (textureWidth > 360) {
		*xLk = 80;
		*xHm = 495;
		*scaleX = *scaleY = 0.800f;
	} else if (textureWidth == 360) {
		*xLk = 80;
		*xHm = 495;
		*scaleX = 1.1f;
		*scaleY = 1.2f;
	} else if (textureWidth == 320) {
		*xLk = 80;
		*xHm = 495;
		*scaleX = *scaleY = 1.2f;
	} else if (textureWidth <= 226) {
		*xLk = 80;
		*xHm = 495;
		*scaleX = *scaleY = 1.7f;
	}

	return;
}

void setTexScaleHomePrew(uint textureWidth, float *scaleX, float *scaleY) {
	if (textureWidth > 900)
		*scaleX = *scaleY = 0.400f;
	else if (textureWidth > 360)
		*scaleX = *scaleY = 0.800f;
	else if (textureWidth == 360) {
		*scaleX = 1.1f;
		*scaleY = 1.2f;
	} else if (textureWidth == 320)
		*scaleX = *scaleY = 1.2f;
	else if (textureWidth <= 226)
		*scaleX = *scaleY = 1.7f;

	return;
}

void setTexScaleThemeInfo(uint textureWidth, float *scaleX, float *scaleY) {
	if (textureWidth > 900)
		*scaleX = *scaleY = 0.200f;
	else if (textureWidth >= 470)
		*scaleX = *scaleY = 0.400f;
	else if (textureWidth >= 360)
		*scaleX = *scaleY = 0.500f;
	else if (textureWidth >= 320)
		*scaleX = *scaleY = 0.600f;
	else if (textureWidth <= 226)
		*scaleX = *scaleY = 0.805f;

	return;
}

void unsetThemeResources(theme_t *themes, const char *type) {
	int cmd = FAST_STRCMP(type, "up") ? 0:(FAST_STRCMP(type, "all") ? 2:1);
	int i = cmd == 0 ? 3:0;
	int len = cmd == 2 ? 6:i+3;

	for (; i<len; ++i) {
		if (themes[i].title != NULL) {
			xmlFree(themes[i].title);
			themes[i].title = NULL;
		}

		if (themes[i].thumb != NULL) {
			vita2d_free_texture(themes[i].thumb);
			themes[i].thumb = NULL;
		}

		if (cmd != 2) {
			int idx = cmd == 0 ? i-3 : i+3;

			themes[i].title = themes[idx].title;
			themes[i].thumb = themes[idx].thumb;
			themes[i].exFlag = themes[idx].exFlag;
			themes[i].installed = themes[idx].installed;
			themes[i].active = themes[idx].active;

			themes[idx].title = NULL;
			themes[idx].thumb = NULL;
		}
	}

	return;
}

void setThemesResources(char **themesList, int themesCount, theme_t *themes, int drawFromNumb) {
	for (int i=drawFromNumb, j=0, len=drawFromNumb+6; i<len && i<themesCount; ++i, ++j) {
		if (themes[j].title != NULL && themes[j].thumb != NULL)
			continue;

		char *themeXmlPath = getThemeXmlPath(themesList[i], 0);
		xmlDoc *doc = xmlReadFile(themeXmlPath, NULL, 0);
		xmlChar *themePrewPath = getThemePreviewPng(themesList[i], doc, "thumb");
		int thmListLen = strlen(themesList[i]);
		int exPLen = thmListLen + 23;
		int activeFLen = strlen(THMX_DATA) + thmListLen + 8;
		char themeExPath[exPLen];

		snprintf(themeExPath, sizeof(char) * exPLen, "ux0:theme/%s/themeEx.xml", themesList[i]);

		if (exists(themePrewPath))
			themes[j].thumb = vita2d_load_PNG_file(themePrewPath);

		themes[j].title = getThemeTitle(doc);
		themes[j].exFlag = exists(themeExPath);
		themes[j].installed = isThemeInstalled(themesList[i]);
		themes[j].active = isActiveTheme(themesList[i]);

		xmlFreeDoc(doc);
		xmlFree(themePrewPath);
		free(themeXmlPath);
	}

	return;
}
