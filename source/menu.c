#include "include/menu.h"

static const char *menu[] = {
    "Install",
    "Bakcup DB",
    "Restore DB",
    "Rebuild DB",
    "Info",
    "Delete",
    "EXify Theme",
    "Unset",
    "Force Unset",
    "Credits"
};

static int menuItmShow[] = {0,0,0,0,0,0,0,0,0,0};
static int menuItmC = 0;
static int menuCurItm = 0;
static int menuStart = 0;
static int menuEnd = 9;
static int menuLen = 10;

/**
 * type: 0 - themes list / 1 - theme page
 */
void setMenu(int type, int isEx, int active) {

    for (int i=0; i<menuLen; ++i)
        menuItmShow[i] = 0;

    switch(type) {
        case 1: {
            menuItmC = 1;
            menuEnd = 5;
            menuCurItm = menuStart = 4;
            menuItmShow[4] = 1;

            if (!active) {
                menuItmShow[5] = 1;
                menuEnd = 5;
                ++menuItmC;
            }

            if (!isEx && !active) {
                menuItmShow[6] = 1;
                menuEnd = 6;
                ++menuItmC;
            }

            if (active) {
                menuItmShow[7] = menuItmShow[8] = 1;
                menuEnd = 8;
                menuItmC += 2;
            }
        }
            break;
        default: { // 0
            menuItmC = 5;
            menuStart = 0;
            menuEnd = 9;
            menuItmShow[0] = menuItmShow[1] = menuItmShow[2] = menuItmShow[3] = menuItmShow[9] = 1;
        }
            break;
    }

    return;
}

int menuDown() {
    if (menuCurItm+1 <= menuEnd) {
        while (!menuItmShow[++menuCurItm]);

        return 1;
    }

    return 0;
}

int menuUp() {
    if (menuCurItm-1 >= menuStart) {
        while (!menuItmShow[--menuCurItm]);

        return 1;
    }

    return 0;
}

void resetMenu() {
    menuCurItm = menuStart;

    return;
}

int getSelMenuItm() {
    return menuCurItm;
}

void drawMenu() {
	float itemH = 50.0f;
	float containerH = (itemH * menuItmC) + 12.5f;
	float itemY = 405.0f;
	float containerY = ((itemY - (itemH * (menuItmC - 1))) - 5.0f) - 0.75f * menuItmC;

	vita2d_draw_rectangle(690.0f, containerY, 240, containerH, RGBA8(240,240,240,180));

	for (int i=menuLen-1; i>=0; --i) {
        if (!menuItmShow[i])
            continue;

		unsigned int color = i == menuCurItm ? RGBA8(210, 55, 55,215):RGBA8(0,0,0,215);

		vita2d_draw_rectangle(695.0f, itemY, 230, itemH, color);
		vita2d_pgf_draw_text(resources.pgf, 701.0f, itemY+37.0f, RGBA8(255, 255, 255,240), 1.8f, menu[i]);

		itemY = floor(itemY - (itemH + 0.75f));
	}

	return;
}