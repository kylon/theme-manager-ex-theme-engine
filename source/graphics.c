#include <psp2/rtc.h>
#include <psp2/power.h>
#include <psp2/ctrl.h>

#include "include/graphics.h"
#include "include/menu.h"

void draw_status_bar() {
	SceDateTime time;
	char timeString[6] = {0};
	int batPerc = scePowerGetBatteryLifePercent();
	uint batPosThrmColor = batPerc > 70 ? RGBA8(30, 179, 45, 255):RGBA8(0, 0, 0, 60);

	sceRtcGetCurrentClockLocalTime(&time);
	snprintf(timeString, sizeof(timeString), "%02d:%02d", sceRtcGetHour(&time), sceRtcGetMinute(&time));

	vita2d_draw_rectangle(0, 0, 960, 30, RGBA8(237,237,237,255)); // status bar
	vita2d_pgf_draw_text(resources.pgf, 814, 23, RGBA8(0,0,0,255), 1.2f, timeString); // time
	vita2d_draw_rectangle(895, 10.5f, 5, 10, batPosThrmColor); // battery: positive thermal
	vita2d_draw_rectangle(900, 4, 45, 22, RGBA8(0,0,0,60)); // battery: black base
	vita2d_draw_rectangle(getBatteryRectX(batPerc), 4, getBatWidth(batPerc), 22, getBatColor(batPerc)); // battery: percentage layer

	return;
}

void draw_title(const char *str, int center) {
	float textX = center ? (960/2)-(strlen(str)*10):20;

	vita2d_pgf_draw_text(resources.pgf, textX, 70, RGBA8(237,237,237,255), 1.5f, str);
	vita2d_draw_line(100, 85, 858, 85, RGBA8(0,0,0,30));

	return;
}

void draw_thmx_main(char **themesList, int themesCnt) {
	SceCtrlData pad, oldPad;
	theme_t themes[6];
	int curSel = 0;
	int drawFromNum = 0;
	float scrollbarPos = 115;
	int menuOpen = 0;
	int deletedFlag = -1;
	int rebootFlag = -1;
	int draw = 1;

	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));
	memset(&themes, 0, sizeof(themes));

	setThemesResources(themesList, themesCnt, themes, drawFromNum);
	setMenu(0, 0, 0);

	while (1) {
		float x = 106;
		float y = 115;
		float rW = 234;
		float rH = 140;
		int j = 0;
		int i = 0;

		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (HOLD_END(oldPad, pad, SCE_CTRL_TRIANGLE)) {
			menuOpen = !menuOpen;
			draw = 1;
			resetMenu();

		} else if (menuOpen) {
			if (IS_BTN(pad, SCE_CTRL_DOWN)) {
				if (menuDown()) {
					draw = 1;
					sceKernelDelayThread(170000);
				}

			} else if (IS_BTN(pad, SCE_CTRL_UP)) {
				if (menuUp()) {
					draw = 1;
					sceKernelDelayThread(170000);
				}

			} else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
				int ret = 0;

				switch (getSelMenuItm()) {
					case 0: {
						if (themesCnt == 0)
							break;

						ret = thmx_dialog(1, "Theme installer\n\n\nScan your theme folder and install new themes.\n\nDo you want to proceed?");
						if (!ret)
							break;

						ret = installAll(themesList, themesCnt);
						if (ret) {
							unsetThemeResources(themes, "all");
							setThemesResources(themesList, themesCnt, themes, drawFromNum);
						}
					}
						break;
					case 1: {
						ret = manageDB(1);
						if (!ret)
							thmx_dialog(0, "Backup failed!");
						else
							thmx_dialog(2, "ux0:data/thmx/shell_bak/app.db");
					}
						break;
					case 2: {
						ret = thmx_dialog(1, "You are about to restore the database.\n\n\nYour device will be rebooted!");
						if (!ret)
							break;

						ret = manageDB(2);
						if (!ret) {
							thmx_dialog(0, "Restore failed!");
							break;
						}

						vita2d_wait_rendering_done();
						unsetThemeResources(themes, "all");
						scePowerRequestColdReset();
						return;
					}
						break;
					case 3: {
						ret = thmx_dialog(1, "You are about to rebuild the database.\n\n"
					                  "This will unset any active theme!\n\n"
					                  "Your System will be rebooted.");
						if (!ret)
							break;

						ret = manageDB(3);
						if (!ret) {
							thmx_dialog(0, "Rebuild failed!");
							break;
						}

						vita2d_wait_rendering_done();
						unsetThemeResources(themes, "all");
						scePowerRequestColdReset();
						return;
					}
						break;
					case 9: {
						draw_credits();
					}
						break;
					default:
						break;
				}
				menuOpen = 0;
				draw = 1;
			}
		} else if (IS_BTN(pad, SCE_CTRL_DOWN)) {
			draw = 1;

			if (curSel+3 > 5) {
				if (drawFromNum < themesCnt-6) {
					drawFromNum += 3;
					scrollbarPos += 20;
					curSel -= 3;

					vita2d_wait_rendering_done();
					unsetThemeResources(themes, "down");
					setThemesResources(themesList, themesCnt, themes, drawFromNum);

					if (themes[curSel].title == NULL)
						curSel = 0;
				}
			} else {
				if (themes[curSel+3].title != NULL)
					curSel += 3;

				sceKernelDelayThread(180000);
			}
		} else if (IS_BTN(pad, SCE_CTRL_UP)) {
			draw = 1;

			if ((curSel-3) < 0) {
				if (drawFromNum > 0) {
					drawFromNum -= 3;
					scrollbarPos -= 20;
					curSel += 3;

					vita2d_wait_rendering_done();
					unsetThemeResources(themes, "up");
					setThemesResources(themesList, themesCnt, themes, drawFromNum);
				}
			} else {
				curSel -= 3;
				sceKernelDelayThread(180000);
			}
		} else if (IS_BTN(pad, SCE_CTRL_LEFT) && curSel != 0 && curSel != 3) {
			draw = 1;
			--curSel;
			sceKernelDelayThread(180000);

		} else if (IS_BTN(pad, SCE_CTRL_RIGHT) && curSel != 2 && curSel != 5 && themes[curSel+1].title != NULL) {
			draw = 1;
			++curSel;
			sceKernelDelayThread(180000);

		} else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS) && drawFromNum+curSel < themesCnt) {
			draw = 1;

			draw_theme_page(themesList, drawFromNum+curSel, &deletedFlag, &rebootFlag, &themes[curSel]);
			setMenu(0, 0, 0);

			if (rebootFlag != -1) {
				vita2d_wait_rendering_done();
				unsetThemeResources(themes, "all");
				scePowerRequestColdReset();
				return;
			}
		}

		oldPad = pad;

		if (deletedFlag != -1) {
			themesCnt = updateThemesList(themesList, themesCnt, deletedFlag);
			deletedFlag = -1;
			drawFromNum = 0;
			scrollbarPos = 115;
			draw = 1;

			vita2d_wait_rendering_done();
			unsetThemeResources(themes, "all");
			setThemesResources(themesList, themesCnt, themes, drawFromNum);
		}

		if (!draw)
			continue;

		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
		draw_status_bar();
		draw_title("Themes", 1);

		while (i<6 && themes[i].title != NULL) {
			float scaleX = 1.0f;
			float scaleY = 1.0f;
			uint textColor = themes[i].installed ? RGBA8(255, 255, 255, 255):RGBA8(25, 6, 6, 130);

			if (xmlStrlen(themes[i].title) > 16) {
				xmlChar *titleSub = xmlStrsub(themes[i].title, 0, 16);
				xmlChar titleF[20];

				snprintf(titleF, sizeof(xmlChar) * 20, "%s...", titleSub);
				vita2d_pgf_draw_text(resources.pgf, (x+2), (y+rH+23), textColor, 1.0f, titleF);
				xmlFree(titleSub);

			} else {
				vita2d_pgf_draw_text(resources.pgf, (x+2), (y+rH+23), textColor, 1.0f, themes[i].title);
			}

			vita2d_draw_rectangle(x, y, rW, rH, RGBA8(237, 237, 237, 50));

			if (themes[i].thumb != NULL) {
				uint textureWidth = vita2d_texture_get_width(themes[i].thumb);

				if (textureWidth > 226)
					scaleX = scaleY = 0.71f;

				vita2d_draw_texture_scale(themes[i].thumb, x+4, y+5.6f, scaleX, scaleY);
			} else {
				vita2d_pgf_draw_text(resources.pgf, x+10, y+(rH/2)+15, RGBA8(255, 255, 255,150), 1.7f, "NO PREVIEW");
			}

			if (themes[i].exFlag == 1) {
				vita2d_pgf_draw_text(resources.pgf, (x+rW)-24, (y+rH), RGBA8(0, 0, 0, 200), 1.4f, "EX"); // shadow
				vita2d_pgf_draw_text(resources.pgf, (x+rW)-25, (y+rH)-1, RGBA8(240, 240, 240, 255), 1.4f, "EX");
			}

			if (isActiveTheme(themesList[drawFromNum+i]))
				vita2d_draw_texture(resources.activeImg, x+50, y+(rH/2)-34);

			if (curSel == i)
				vita2d_draw_fill_circle(x+5, y+5, 18, RGBA8(232, 204, 23, 255));

			x = (j==2) ? 106:(x+rW+20);
			y = (j==2) ? (y+rH+75):y;
			j = (j==2) ? 0:(j+1);
			++i;
		}

		if (themesCnt > 6)
			draw_scrollbar(scrollbarPos, themesCnt);

		if (menuOpen)
			drawMenu();

		vita2d_end_drawing();
		vita2d_swap_buffers();

		draw = 0;
	}

	return;
}

void draw_theme_page(char **themesList, int themeIdx, int *deletedFlag, int *rebootFlag, theme_t *theme) {
	char *themeXmlPath = getThemeXmlPath(themesList[themeIdx], 0);
	vita2d_texture *lockscreen, *home;
	uint textureWidth = 0;
	float textureXLock = 106.0f;
	float textureXHome = 535.0f;
	float textureScaleXLock = 1.0f;
	float textureScaleYLock = 1.0f;
	float textureScaleXHome = 1.0f;
	float textureScaleYHome = 1.0f;
	int menuOpen = 0, draw = 1;
	SceCtrlData pad, oldPad;
	xmlChar *lockPrew, *homePrew;

	xmlDoc *doc = xmlReadFile(themeXmlPath, NULL, 0);

	if (doc == NULL) {
		int len = strlen(themeXmlPath) + 34;
		char msg[len];

		snprintf(msg, sizeof(char) * len, "Theme: Cannot open theme.xml!\n\n%s", themeXmlPath);
		thmx_dialog(0, msg);
		return;
	}

	lockPrew = getThemePreviewPng(themesList[themeIdx], doc, "lockPrew");
	homePrew = getThemePreviewPng(themesList[themeIdx], doc, "homePrew");
	lockscreen = vita2d_load_PNG_file(lockPrew);
	home = vita2d_load_PNG_file(homePrew);

	if (lockscreen != NULL) {
		textureWidth = vita2d_texture_get_width(lockscreen);
		setTexScaleAndCoordCommon(textureWidth, &textureXLock, &textureXHome, &textureScaleXLock, &textureScaleYLock);
	}

	if (home != NULL) {
		textureWidth = vita2d_texture_get_width(home);
		setTexScaleHomePrew(textureWidth, &textureScaleXHome, &textureScaleYHome);
	}

	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));

	xmlFreeDoc(doc);
	xmlFree(lockPrew);
	xmlFree(homePrew);

	setMenu(1, theme->exFlag, theme->active);

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (HOLD_END(oldPad, pad, SCE_CTRL_TRIANGLE)) {
			menuOpen = !menuOpen;
			draw = 1;
			resetMenu();

		} else if (menuOpen) {
			if (IS_BTN(pad, SCE_CTRL_UP)) {
				if (menuUp()) {
					draw = 1;
					sceKernelDelayThread(180000);
				}
			} else if (IS_BTN(pad, SCE_CTRL_DOWN)) {
				if (menuDown()) {
					draw = 1;
					sceKernelDelayThread(180000);
				}
			} else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
				int ret = 0;

				switch (getSelMenuItm()) {
					case 4: {
						draw_theme_info(lockscreen, home, themeXmlPath, theme);
					}
						break;
					case 5: {
						int idLen = strlen(themesList[themeIdx]) + 11;
						char id[idLen];
						char msg[91] = {0};

						if (xmlStrlen(theme->title) > 50) {
							xmlChar *title = xmlStrsub(theme->title, 0, 47);
							int ellips = sizeof(char) * 3;

							snprintf(msg, sizeof(msg) + ellips, "%s...\n\n\nDo you want to delete this theme?", title);
							xmlFree(title);

						} else {
							snprintf(msg, sizeof(msg), "%s\n\n\nDo you want to delete this theme?", theme->title);
						}

						ret = thmx_dialog(1, msg);
						if (!ret)
							break;

						snprintf(id, sizeof(char) * idLen, "ux0:theme/%s", themesList[themeIdx]);

						ret = delTheme(id, theme->installed);
						if (ret)
							*deletedFlag = themeIdx;

						if (ret)
							goto themePage_exit;
					}
						break;
					case 6: {
						ret = thmx_dialog(1, "THEME DEV MODE\n\nDo you want to EXify this theme?");
						if (!ret)
							break;

						ret = exify(themesList[themeIdx]);
						if (ret) {
							theme->exFlag = 1;
							setMenu(1, theme->exFlag, theme->active);
						}
					}
						break;
					case 7: {
						ret = thmx_dialog(1, "This will unset the active theme.\n\n"
						                  "Your device will be rebooted for the changes to take effect!");
						if (!ret)
							break;

						ret = unsetTheme();
						if (theme->exFlag)
							ret = unsetExTheme(theme->exFlag);
#ifdef THMXD
						shipLogDumpToDisk();
#endif
						if (ret) {
							*rebootFlag = 1;
							goto themePage_exit;
						}
					}
						break;
					case 8: {
						ret = thmx_dialog(1, "Force unset\n\n\n"
						                  "1) Unset the active theme.\n\n"
						                  "2) Rebuild your database for further clean up.\n\n"
						                  "3) Your system will be rebooted\n\n\nDo you want to proceed?");
						if (!ret)
							break;

						ret = unsetTheme();
						if (theme->exFlag && ret)
							ret = unsetExTheme();

						if (ret) {
							ret = manageDB(3);
							if (!ret) {
								thmx_dialog(0, "Database rebuild failed!");
								break;
							}

							*rebootFlag = 1;
							goto themePage_exit;
						}
					}
						break;
					default:
						break;
				}
				menuOpen = 0;
				draw = 1;
			}
		} else if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE)) {
			sceKernelDelayThread(140000);
			goto themePage_exit;

		} else if (!theme->active && theme->installed && HOLD_END(oldPad, pad, SCE_CTRL_SQUARE)) {
			int ret;

			draw = 1;
			ret = thmx_dialog(1, "Do you want to apply this theme?\n\nYour device will be rebooted for the changes to take effect!");
			if (ret) {
				ret = doApplyTheme(themesList[themeIdx], theme->exFlag);
#ifdef THMXD
				shipLogDumpToDisk();
#endif
				if (!ret) {
					thmx_dialog(0, "Something went wrong, please try to rebuild your database.");

				} else {
					*rebootFlag = 1;
					goto themePage_exit;
				}
			}
		}

		oldPad = pad;

		if (!draw)
			continue;

		vita2d_start_drawing();
		vita2d_clear_screen();

		vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
		draw_status_bar();
		draw_title(theme->title, 0);

		if (lockscreen != NULL) {
			vita2d_draw_texture_scale(lockscreen, textureXLock, 150.0f, textureScaleXLock, textureScaleYLock);
		} else {
			vita2d_draw_rectangle(86.0f, 150.0f, 380, 217, RGBA8(237, 237, 237, 50));
			vita2d_pgf_draw_text(resources.pgf, 154.0f, 267.0f, RGBA8(255, 255, 255, 150), 1.9f, "NO PREVIEW");
		}

		if (home != NULL) {
			vita2d_draw_texture_scale(home, textureXHome, 150.0f, textureScaleXHome, textureScaleYHome);
		} else {
			vita2d_draw_rectangle(505.0f, 150.0f, 380, 217, RGBA8(237, 237, 237, 50));
			vita2d_pgf_draw_text(resources.pgf, 570.0f, 267.0f, RGBA8(255, 255, 255, 160), 1.9f, "NO PREVIEW");
		}

		if (theme->installed) {
			const char *btnLabel = theme->active ? "Applied":"[ ] Apply";

			vita2d_draw_rectangle((960 / 2) - 145, 400.0f, 300, 55, RGBA8(240, 240, 240, 80));
			vita2d_pgf_draw_text(resources.pgf, 415.0f, 438.0f, RGBA8(255, 255, 255, 230), 1.9f, btnLabel);

		} else {
			vita2d_pgf_draw_text(resources.pgf, 273.0f, 441.0f, RGBA8(255, 255, 255, 230), 1.7f, "This theme is not installed!");
		}

		if (menuOpen)
			drawMenu();

		vita2d_end_drawing();
		vita2d_swap_buffers();

		draw = 0;
	}

themePage_exit:
	vita2d_wait_rendering_done();
	vita2d_free_texture(lockscreen);
	vita2d_free_texture(home);
	free(themeXmlPath);

	return;
}

void draw_theme_info(const vita2d_texture *lockPrew, const vita2d_texture *homePrew, const char *themeXmlPath, const theme_t *theme) {
	uint textureWidth = 0;
	int folderPathLen = strlen(themeXmlPath) - 9;
	char themeFolderPath[folderPathLen + 1];
	const char *type = theme->exFlag ? "Extended":"Standard";
	int themeSz = 0, draw = 1;
	SceCtrlData pad, oldPad;

	xmlDoc *doc = xmlReadFile(themeXmlPath, NULL, 0);

	if (doc == NULL) {
		int len = strlen(themeXmlPath) + 39;
		char msg[len];

		snprintf(msg, sizeof(char) * len, "Theme Info: Cannot open theme.xml!\n\n%s", themeXmlPath);
		thmx_dialog(0, msg);
		return;
	}

	xmlChar *author = getThemeProp(doc, "author");
	xmlChar *version = getThemeProp(doc, "version");
	int infoLen = xmlStrlen(author) + xmlStrlen(version) + 60;
	char infoText[infoLen];

	snprintf(themeFolderPath, sizeof(char) * (folderPathLen + 1), themeXmlPath, themeXmlPath+folderPathLen);
	themeSz = getThemeSize(themeFolderPath);
	snprintf(infoText, sizeof(char) * infoLen, "Author: %s\n\nVersion: %s\n\nSize: %d KB\n\nType: %s", author, version, themeSz/1024, type);

	xmlFreeDoc(doc);
	xmlFree(author);
	xmlFree(version);

	vita2d_start_drawing();
	vita2d_clear_screen();

	vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
	draw_status_bar();
	draw_title(theme->title, 0);

	if (lockPrew != NULL) {
		float textureScaleXLock = 0.0f;
		float textureScaleYLock = 0.0f;

		textureWidth = vita2d_texture_get_width(lockPrew);

		setTexScaleThemeInfo(textureWidth, &textureScaleXLock, &textureScaleYLock);
		vita2d_draw_texture_scale(lockPrew, 300, 100, textureScaleXLock, textureScaleYLock);

	} else {
		vita2d_draw_rectangle(290, 100, 200, 115, RGBA8(237, 237, 237, 50));
		vita2d_pgf_draw_text(resources.pgf, 315, 165, RGBA8(255, 255, 255,150), 1.2f, "NO PREVIEW");
	}

	if (homePrew != NULL) {
		float textureScaleXHome = 0.0f;
		float textureScaleYHome = 0.0f;

		textureWidth = vita2d_texture_get_width(homePrew);

		setTexScaleThemeInfo(textureWidth, &textureScaleXHome, &textureScaleYHome);
		vita2d_draw_texture_scale(homePrew, 500, 100, textureScaleXHome, textureScaleYHome);

	} else {
		vita2d_draw_rectangle(500, 100, 200, 115, RGBA8(237, 237, 237, 50));
		vita2d_pgf_draw_text(resources.pgf, 522, 165, RGBA8(255, 255, 255,150), 1.2f, "NO PREVIEW");
	}

	vita2d_pgf_draw_text(resources.pgf, 20, 270, RGBA8(255, 255, 255,240), 1.7f, infoText);

	vita2d_end_drawing();
	vita2d_swap_buffers();

	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE)) {
			sceKernelDelayThread(160000);
			break;
		}

		oldPad = pad;
	}

	return;
}

void draw_credits() {
	SceCtrlData pad, oldPad;

	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));

	vita2d_start_drawing();
	vita2d_clear_screen();

	vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
	draw_status_bar();
	draw_title("Credits", 1);

	vita2d_pgf_draw_text(resources.pgf, 20, 120, RGBA8(255, 255, 255,240), 1.2f, "Theme Manger Ex by Kylon\n\n\n"
			"Team Molecule: Henkaku\n\n"
			"Xerpi: vita2d\n\nxyz: Sqlite3\n\n"
			"TheFlow: VitaShell code\n\n"
			"dots-tb and CelesteBlue-dev: PSVita-RE-tools\n\n"
			"VitaSmith: libsqlite\n\n"
			"All iirc members for the help.");

	vita2d_end_drawing();
	vita2d_swap_buffers();

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE)) {
			sceKernelDelayThread(160000);
			break;
		}

		oldPad = pad;
	}

	return;
}

void draw_scrollbar(float pos, int themesCnt) {
	int height = 390 - (20 * (ceil(themesCnt/3)));

	if (height <= 5)
		height = 10;

	vita2d_draw_rectangle(950, pos, 10, height, RGBA8(237,237,237,75));

	return;
}

int thmx_dialog(int type, const char *msg) {
	SceCtrlData pad, oldPad;

	memset(&pad, 0, sizeof(pad));
	memset(&oldPad, 0, sizeof(oldPad));

	switch (type) {
		case 2: // success
		case 0: { // error
			const char *status = type == 0 ? "Error!":"Success!";

			vita2d_start_drawing();
			vita2d_clear_screen();

			vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
			draw_status_bar();

			vita2d_draw_rectangle(100, 60, 770, 450, RGBA8(0,0,0,200)); // error rectangle
			vita2d_pgf_draw_text(resources.pgf, 415, 100, RGBA8(255, 255, 255,240), 1.6f, status);
			vita2d_pgf_draw_text(resources.pgf, 118, 158, RGBA8(255, 255, 255,240), 1.1f, msg);
			vita2d_draw_rectangle(350, 420, 250, 55, RGBA8(240,240,240,160)); // btn ok
			vita2d_pgf_draw_text(resources.pgf, 420, 460, RGBA8(0, 0, 0,200), 1.8f, "(X) OK");

			vita2d_end_drawing();
			vita2d_swap_buffers();

			while (1) {
				sceCtrlPeekBufferPositive(0, &pad, 1);

				if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
					sceKernelDelayThread(160000);
					break;
				}

				oldPad = pad;
			}
		}
			break;
		case 1: { // yes/no
			vita2d_start_drawing();
			vita2d_clear_screen();

			vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
			draw_status_bar();
			vita2d_draw_rectangle(100, 60, 770, 450, RGBA8(0,0,0,200));
			vita2d_pgf_draw_text(resources.pgf, 110, 100, RGBA8(255, 255, 255,240), 1.3f, msg);
			vita2d_draw_rectangle(100, 420, 385, 90, RGBA8(255,255,255,230)); // btn ok
			vita2d_pgf_draw_text(resources.pgf, 230, 475, RGBA8(0, 0, 0,200), 2.0f, "(X) Yes");
			vita2d_draw_rectangle(490, 420, 380, 90, RGBA8(255,255,255,230)); // btn cancel
			vita2d_pgf_draw_text(resources.pgf, 607, 475, RGBA8(0, 0, 0,200), 2.0f, "(O) No");

			vita2d_end_drawing();
			vita2d_swap_buffers();

			while (1) {
				sceCtrlPeekBufferPositive(0, &pad, 1);

				if (HOLD_END(oldPad, pad, SCE_CTRL_CIRCLE)) {
					sceKernelDelayThread(160000);
					return 0;

				} else if (HOLD_END(oldPad, pad, SCE_CTRL_CROSS)) {
					sceKernelDelayThread(160000);
					return 1;
				}

				oldPad = pad;
			}
		}
			break;
		default:
			break;
	}

	return 0;
}

void draw_progress(int percent, const char *msg) {
	char percentC[5] = {0};
	float rWidth = (percent/100.0f) * 960.0f;

	snprintf(percentC, sizeof(percentC), "%d%%", percent);

	vita2d_start_drawing();
	vita2d_clear_screen();

	vita2d_draw_texture(resources.bgImage, 0.0f, 0.0f);
	draw_status_bar();

	vita2d_pgf_draw_text(resources.pgf, 420, 220, RGBA8(255, 255, 255, 240), 2.8f, percentC);
	vita2d_pgf_draw_text(resources.pgf, 20, 360, RGBA8(255, 255, 255, 240), 2.0f, msg);
	vita2d_draw_rectangle(0, 380, rWidth, 170, RGBA8(180, 54, 29, 160));

	vita2d_end_drawing();
	vita2d_swap_buffers();

	return;
}
