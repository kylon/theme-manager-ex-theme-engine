#pragma once

#include "utils.h"

char *getAttributesString(xmlNode *node);
int parseAndInstallTemplate(const char *templatePath, const char *titleId, sqlite3 *db, const char *lang);
