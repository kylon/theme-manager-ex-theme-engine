#pragma once

#include "graphics.h"

void setMenu(int type, int isEx, int active);
int menuUp();
int menuDown();
void resetMenu();
int getSelMenuItm();
void drawMenu();