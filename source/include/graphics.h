#pragma once

#include "gfx_utils.h"

#define HOLD_END(oldPad, pad, control) ((oldPad.buttons & control) && (!(pad.buttons & control)))
#define IS_BTN(pad, control) (pad.buttons & control)

struct commonRes {
	vita2d_pgf *pgf;
	vita2d_texture *bgImage;
	vita2d_texture *activeImg;
	int sysLanguage;
} resources;

void draw_status_bar();
void draw_title(const char *str, int center);
void draw_thmx_main(char **themesList, int themesCount);
void draw_theme_page(char **themesList, int themeIdx, int *deleteFlag, int *rebootFlag, theme_t *theme);
void draw_theme_info(const vita2d_texture *lockPrew, const vita2d_texture *homePrew, const char *themeXmlPath, const theme_t *theme);
void draw_credits();
void draw_scrollbar(float pos, int themesCount);
int thmx_dialog(int type, const char *msg);
void draw_progress(int percent, const char *msg);
