#pragma once

#define SCE_ERROR_ERRNO_EEXIST 0x80010011

int exists(const char *path);
int touchFile(const char *path);
int isDir(const char *path);
int mkDir(const char *path, int mode);
int ioRemove(const char *path);
int copyFile(const char *src, const char *dest);
int copyDir(const char *src, const char *dest);
int countFolders(const char *path);