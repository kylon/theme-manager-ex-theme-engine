#pragma once

#include "utils.h"

int getBatteryRectX(int percentage);
int getBatWidth(int percentage);
uint getBatColor(int percentage);
void setTexScaleAndCoordCommon(uint textureWidth, float *xLk, float *xHm, float *scaleX, float *scaleY);
void setTexScaleHomePrew(uint textureWidth, float *scaleX, float *scaleY);
void unsetThemeResources(theme_t *themes, const char *type);
void setThemesResources(char **themesList, int themesCount, theme_t *themes, int drawFromNumb);
void setTexScaleThemeInfo(uint textureWidth, float *scaleX, float *scaleY);
