#pragma once

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <vita2d.h>
#include <psp2/io/dirent.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/registrymgr.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#ifdef THMXD
#include <psp2/kernel/clib.h>
#endif

#include "sqlite3.h"
#include "file.h"
#include "template_utils.h"
#include "thmx_ids.h"

#define DEFAULT_THEME             "vs0:data/internal/theme"
#define APPDB                     "ur0:shell/db/app.db"
#define THMX_DATA                 "ux0:data/thmx"

#define FAST_STRCMP(str1, str2) (str1[0] == str2[0] && !strcmp(str1, str2))

typedef struct theme {
	xmlChar *title;
	vita2d_texture *thumb;
	int installed;
	int exFlag;
	int active;
} theme_t;

// mount - Credits TheOfficialFlow
int vshIoUmount(int id, int a2, int a3, int a4);
int _vshIoMount(int id, const char *path, int permission, void *buf);
int vshIoMount(int id, const char *path, int permission, int a4, int a5, int a6);
int remount(int id, int perms);

// init
void themeManagerExInit();
char **getThemesListAndCount(int *totThemes);

// XML
xmlChar *getThemeProp(xmlDoc *doc, const char *what);
xmlChar *parseNodes(xmlNode *a_node, const char *what);
char *getThemeXmlPath(const char *themeFolder, int ex);
xmlChar *getThemeTitle(xmlDoc *doc);
xmlChar *getThemePreviewPng(const char *themeFolder, xmlDoc *doc, const char *imageType);
char *getTitleId(const char *nodeName);

// themes
int getThemeSize(const char *themePath);
int delTheme(const char *id, int isInstalled);
int applyTheme(const char *themeFolder);
int applyExTheme(const char *themeFolder);
int unsetTheme();
int unsetExTheme();
int doApplyTheme(const char *themeFolder, int isEx);
int setActiveFlag(const char *themeFolder);
int isActiveTheme(const char *themeFolder);
int isThemeInstalled(const char *themeFolder);
int exify(const char *themeFolder);
int installAll(char **themesList, int themesCount);

// sqlite
int sql_exec(sqlite3 *db, const char *query, const char *funcToLog);
char *createTimeStamp();
char *genSqliteBlob(const char *str);
int genVersionNumb(const char *str);
void _add_rm_list(sqlite3_context *context, int argc, sqlite3_value **argv);

// utils
int manageDB(int cmd);
int updateThemesList(char **themesList, int themesCount, int deletedIdx);
int handleDynLiveArea(const char *themeOverridePath, const char *titleId, sqlite3 *db, const char *mode);
void themesListFree(char **themesList, int count);
char *getLangString();

// shiplog
void shipLogDumpToDisk();
