#include "include/graphics.h"

int main() {
	extern unsigned char _binary_source_img_bg_png_start;
	extern unsigned char _binary_source_img_active_png_start;
	char **themesList = NULL;
	int themesC = 0;

	themeManagerExInit();
	vita2d_init();

	resources.pgf = vita2d_load_default_pgf();
	resources.bgImage = vita2d_load_PNG_buffer(&_binary_source_img_bg_png_start);
	resources.activeImg = vita2d_load_PNG_buffer(&_binary_source_img_active_png_start);

	themesList = getThemesListAndCount(&themesC);
	draw_thmx_main(themesList, themesC);

	vita2d_fini();
	vita2d_free_pgf(resources.pgf);
	vita2d_free_texture(resources.bgImage);
	vita2d_free_texture(resources.activeImg);
	themesListFree(themesList, themesC);

	sceKernelExitProcess(0);
	return 0;
}
